#!/usr/bin/env perl

=pod

=head1 NAME

itaggerDereplicate.pl

=head1 DESCRIPTION

Pool samples, generate FASTA files of all and high-quality only read sequences (lowQual.fasta and highQual.fasta, respectively).  Dereplicate high-quality sequences, relabel them to />Uniq\d+;size=\d+;/, sort by decreasing abundance, and output to uniq.fasta.

This script uses the open-source VSEARCH, a single thread, and more RAM than required by read QC.

=head1 OPTIONS

=over 5

=item --infile C<file>

List of basenames of input to concatenate and dereplicate, where infiles are C<basename>.highQ.fa.gz and C<basename>.lowQ.fa.gz, as produced by C<iTaggerReadQc.pl>.

=item --high C<file>

All high-quality reads in one FASTA outfile.

=item --low C<file>

All low-quality reads in one FASTA outfile.

=item --uniq C<file>

Dereplicated reads FASTA outfile.  Only high-quality reads are used.

=item --config C<file>

Configuration file in C<.ini> format.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item --threads C<int>

Number of threads.  Optional; default = 1.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

This pipeline wraps VSEARCH; see: https://github.com/torognes/vsearch

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Env qw(TMPDIR ITAGGER_CONFIG_DIR);
use Getopt::Long;
use Pod::Usage;
use Config::Simple;
use JSON;
use File::Slurp;
use File::Which;

our $VERSION = '3.0';
our $vers = 1;
our $json = new JSON;
our %log = ();

# OPTIONS
my $configFile;
my $infile;
my $lowQualOutfile;
my $highQualOutfile;
my $uniqOutfile;
my $logfile;
my $threads = 1;
my $overwrite;
my $help;
my $man;
my $test;
GetOptions(
    'config=s' => \$configFile,
	'infile=s' => \$infile,
	'high=s' => \$highQualOutfile,
	'low=s' => \$lowQualOutfile,
	'uniq=s' => \$uniqOutfile,
	'log=s' => \$logfile,
	'threads=i' => \$threads,
	'overwrite' => \$overwrite,
    'help|?' => \$help,
    'man' => \$man,
	'test' => \$test
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
$test and test();

# VALIDATE INPUT
$configFile or die("--config file required\n");
$infile or die("--in file required\n");
$lowQualOutfile or die("--low reads outfile required\n");
$highQualOutfile or die("--high qual reads outfile required\n");
$uniqOutfile or die("--uniq reads outfile required\n");
$logfile or die("--log file required\n");

# LOG
$logfile =~ s/\.done$//i;
our $failLogFile = "$logfile.FAIL"; # created upon failure
our $passLogFile = "$logfile.done"; # created upon success
-e $failLogFile and unlink($failLogFile);
if ( -e $passLogFile and ! $overwrite and -e $uniqOutfile )
{
	warn("Not overwriting previous results\n");
	exit;
} else
{
	unlink($passLogFile, $uniqOutfile);
}
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");

# CONCATENATE INPUT
$verbose and print "Concatenating infiles\n";
my $numHighQual;
my $numInput;
eval { ($numHighQual, $numInput) = concatenate($infile, $highQualOutfile, $lowQualOutfile, \%log); };
if ( $@ ) { fail("CONCATENATE INFILES FAILED: $@\n") }
unless ( $numHighQual )
{
	done();
	exit;
}

# CALCULATE MIN UNIQ SIZE
my $minUniqSize = 2;
my $minUniqCutoff = $config->param('DEREPLICATION.MIN_UNIQ_CUTOFF');
defined($minUniqCutoff) and $minUniqSize = int($minUniqCutoff * $numHighQual + 0.5);
$log{SIZE_MIN} = $minUniqSize;

# DEREPLICATE
$verbose and print "Dereplicating; minUniqSize=$minUniqSize\n";
my $numUniq;
eval { $numUniq = dereplicate($highQualOutfile, $uniqOutfile, \%log, $minUniqSize, $threads, $numHighQual); };
if ($@) { fail("DEREPLICATION FAILED: $@\n") }

# END
done();
exit;

sub done
{
	write_file($passLogFile, $json->pretty->encode(\%log));
	exit;
}

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler { fail("INTERRUPTED: $!") }

# Concatenate sequences listed in input file.  If the input list should contain all the *.highQ.fasta files and the list of *.lowQ.fasta files will automatically be generated from it.
sub concatenate
{
	my ($infile, $highQualOutfile, $lowQualOutfile, $log) = @_;
	my @files = read_file($infile, chomp => 1);
	$log->{NUM_INFILES} = scalar(@files);
	my ($numHighQual, $numLowQual) = (0,0);

	# CONCATENATE HIGH-QUALITY FILES
	my $in;
	open(my $highFH, '>', $highQualOutfile) or die($!);
	foreach my $base (@files)
	{
		my $path = "$base.highQ.fa.gz";
		-f $path or die("File not found: $path\n");
		open($in, "zcat $path|") or die("Unable to open $path: $!");
		while (<$in>)
		{
			print $highFH $_;
			/^>/ and ++$numHighQual;
		}
		close($in);
	}
	close($highFH);

	# CONCATENATE HIGH-QUALITY FILES
	open(my $lowFH, '>', $lowQualOutfile) or die($!);
	foreach my $base (@files)
	{
		my $path = "$base.lowQ.fa.gz";
		-f $path or next;
		open($in, "zcat $path|") or die("Unable to open $path: $!");
		while (<$in>)
		{
			print $lowFH $_;
			/^>/ and ++$numLowQual;
		}
		close($in);
	}
	close($lowFH);

	my $numTotal = $log->{NUM_INPUT} = $numHighQual + $numLowQual;
	#$log->{NUM_LOW_QUAL} = $numLowQual;
	$log->{NUM_HIGH_QUAL} = $numHighQual;
	$log->{PCT_HIGH_QUAL} = $numTotal ? int($numHighQual/$numTotal*100+0.5) : undef;
	return $numHighQual;
}

# Dereplicate reads using VSEARCH.  Reads must have sample= defined in header.  Multithreading supported.
sub dereplicate
{
	my ($infile, $outfile, $log, $minUniqSize, $threads, $numInput) = @_;
	($infile and $outfile and defined($log)) or die("Missing arguments");
	$minUniqSize or $minUniqSize = 2;
	$threads or $threads = 1;
	my $cmd = "vsearch -derep_fulllength $infile -output $outfile -threads $threads -sizeout -relabel Uniq -minuniquesize $minUniqSize";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running command: $cmd\n$output\n");
	my $numUniq;
	if ( $output =~ /(\d+) unique sequences, avg cluster \d+\.\d+, median \d+, max (\d+)/ )
	{
		$numUniq = $log->{NUM_UNIQ} = $1 + 0;
		$log->{SIZE_MAX} = $2 + 0;
		$numInput and $log->{PCT_UNIQ} = int($numUniq/$numInput*100+0.5);
	}
	return $numUniq;
}

#
sub test
{
1;  # TODO	

}
