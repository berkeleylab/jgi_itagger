#!/usr/bin/env perl

=pod

=head1 NAME

itaggerMSA.pl

=head1 DESCRIPTION

Generate multiple sequence alignment.

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration file in C<ini> format.

=item --in C<file>

Input FASTA file.

=item --out C<file>

Multiple sequence alignment outfile.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item --threads C<int>

Number of threads.  Optional; default=32.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

This script wraps MAFFT and QIIME's make_phylogeny.py; refer to their documentation for author, citation, and copyright information.

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use Config::Simple;
use JSON;
use File::Slurp;

our $VERSION = '3.0';
our $start = time;
our %log = ();
our $threads = 32;
our $json = new JSON;

# OPTIONS
my $infile;
my $outfile;
my $configFile;
my $logfile;
my $help;
my $man;
GetOptions(
	'in=s' => \$infile,
	'out=s' => \$outfile,
    'config=s' => \$configFile,
	'log=s' => \$logfile,
	'threads=i' => \$threads,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$infile or die("--in file required\n");
$outfile or die("--out file required\n");
$logfile or die("--log file required\n");
$configFile or die("--config file required\n");

# LOG
$logfile =~ s/\.done$//i;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
-e $passLogFile and unlink($passLogFile);
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");
my $mafftOpt = $config->param('MSA.MAFFT_OPT') or die("Config missing required MSA/MAFFT_OPT parameter\n");

# RUN
my $cmd="mafft --thread $threads --quiet $mafftOpt $infile > $outfile";
system($cmd) == 0 or fail("ERROR running $cmd");
done();

sub done
{
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($passLogFile, $json->pretty->encode(\%log));
	exit;
}

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler { fail("INTERRUPTED: $!") }
