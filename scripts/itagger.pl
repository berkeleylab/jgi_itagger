#!/usr/bin/env perl

=pod

=head1 NAME

itagger.pl

=head1 DESCRIPTION

Pipeline for amplicon analysis (e.g. 16S rRNA, fungal ITS), which is basically the USEARCH and QIIME pipelines.  Support for multiple clustering and taxonomic classification types.

=head1 SYNOPSIS

    itagger.pl --config C<path> --in C<file> --out C<folder>

=head1 OPTIONS

=over 5

=item --config C<path>

Config file in C<INI> format.

=item --in C<file>

Tab-delimited text file: C<sampleId, FASTQ file>

=item --out C<folder>

Base folder for output.

=item --threads C<int>

Number of threads to use.  Optional; default=nproc (all).

=back

=head1 AUTHORS

Originally created by Julien Tremblay (julien.tremblay@mail.mcgill.ca).
Now maintained and developed by Edward Kirton (ESKirton@LBL.gov).

=head1 COPYRIGHT

Copyright (c) 2013 United States Department of Energy Joint Genome Institute.  Use freely under the same license as Perl itself.  Refer to wrapped tools for their own licenses and copyright information.

=cut

use strict;
use warnings;
use Env qw/TMPDIR ITAGGER_CONFIG_DIR/;
use Getopt::Long;
use Pod::Usage;
use Parallel::ForkManager;
use File::Path qw(remove_tree make_path);
use File::Copy;
use JSON;
use File::Slurp;
use File::Which;
use File::Basename;

our $VERSION = "3.0";
$|=1;

# VALIDATE ENV
die("\$TMPDIR not defined") unless defined($TMPDIR);
die("\$TMPDIR does not exist: $TMPDIR") unless -d $TMPDIR;

# OPTIONS
my ($help, $man, $configFile, $infile, $dir);
my $threads = `nproc`;
chomp $threads;
GetOptions(
    'config=s' => \$configFile,
    'in=s' => \$infile,
    'out=s' => \$dir,
    'threads=i' => \$threads,
    'help' => \$help,
    'man' => \$man ) or pod2usage(2);
pod2usage(1) if $help;
pod2usage( -exitstatus => 0, -verbose => 2 ) if $man;
die("--threads required\n") unless $threads;
$configFile or die("--config file required\n");
$infile or die("--in file required\n");
$dir or die("--out dir required\n");
$threads or $threads = `nproc` + 0;
our $pm = new Parallel::ForkManager($threads);

# INIT VARS
my $cmd;

# READ INFILE
my $readsDir = "$dir/READS";
-d $readsDir or make_path($readsDir) or die("Unable to mkdir $readsDir: $!\n");
my $readsFile = "$dir/READS/reads.txt";
my %samples;
open(my $in, '<', $infile) or die($!);
open(my $out, '>', $readsFile) or die($!);
while (<$in>)
{
	chomp;
	my ($sampleId, $fastqFile) = split(/\t/);
	$sampleId =~ s/[^A-z0-9\.]/_/g;
	$sampleId =~ /^[A-z]/ or $sampleId = "S$sampleId";
	-s $fastqFile or die("File not found: $fastqFile\n");
	my ($basename, $fastqDir) = File::Basename::fileparse($fastqFile, '.fastq.gz');
	my $logfile = "$readsDir/$basename.done";
	my $hiOut = "$readsDir/$basename.highQ.fa.gz";
	print $out "$readsDir/$basename\n";
	my $lowOut = "$readsDir/$basename.lowQ.fa.gz";
	$samples{$sampleId} = [ $fastqFile, $logfile, $hiOut, $lowOut ];
}
close($in);

# READ QC
foreach my $sampleId ( sort keys %samples )
{
	$pm->start and next;
	my ( $fastqFile, $logfile, $hiOut, $lowOut ) = @{$samples{$sampleId}};
	if ( -e $logfile and -e $hiOut )
	{
		warn("Using existing read QC file: $hiOut\n");
	} else
	{
		$cmd = "itaggerReadQc.pl --config $configFile --sample $sampleId --in $fastqFile --high $hiOut --low $lowOut --log $logfile";
		print $cmd, "\n";
		system($cmd) == 0 or die("ERROR (sample=$sampleId): $!\n");
	}
	$pm->finish;
}
$pm->wait_all_children;

# DEREPLICATION
my $allReadsFile = "$readsDir/allReads.fasta";
my $highQualReads = "$readsDir/hiQualReads.fasta";
my $lowQualReads = "$readsDir/lowQualReads.fasta";
my $uniqReads = "$readsDir/uniq.fasta";
my $logfile = "$readsDir/DEREPLICATION.done";
if ( -e $logfile and -e $uniqReads )
{
	warn("Using existing dereplication file: $uniqReads\n");
} else
{
	$cmd = "itaggerDereplication.pl --config $configFile --infile $readsFile --low $lowQualReads --high $highQualReads --uniq $uniqReads --log $logfile --threads $threads";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

# CLUSTERING
my $otuDir = "$dir/OTU";
-d $otuDir or make_path($otuDir) or die("Unable to mkdir $otuDir: $!\n");
my $clustersFile = "$otuDir/all.otu.fasta";
$logfile = "$otuDir/CLUSTERING.done";
if ( -e $logfile and -e $clustersFile )
{
	warn("Using existing clusters file: $clustersFile\n");
} else
{
	$cmd = "itaggerClustering.pl --config $configFile --in $uniqReads --out $clustersFile --log $logfile --threads $threads";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

# MAP READS
my $countsFile = "$otuDir/all.counts.txt";
$logfile = "$otuDir/MAPPING.done";
if ( -e $logfile and -e $countsFile )
{
	warn("Using existing mapping file: $countsFile\n");
} else
{
	$cmd = "itaggerMapping.pl --conf $configFile --db $clustersFile --out $countsFile --log $logfile --threads $threads --in $highQualReads --in $lowQualReads";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

# CLASSIFICATION
my $taxFile = "$otuDir/all.tax.txt";
$logfile = "$otuDir/TAXONOMY.done";
if ( -e $logfile and -e $taxFile )
{
	warn("Using existing taxonomy file: $taxFile\n");
} else
{
	$cmd = "itaggerTaxonomy.pl --config $configFile --in $clustersFile --out $taxFile --log $logfile --threads $threads";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

# MSA
my $msaFile = "$otuDir/all.msa.fasta";
$logfile = "$otuDir/MSA.done";
if ( -e $logfile and -e $msaFile )
{
	warn("Using existing multiple sequence alignment: $msaFile\n");
} else
{
	$cmd = "itaggerMSA.pl --config $configFile --in $clustersFile --out $msaFile --log $logfile --threads $threads";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

# CONTAM FILTER
my $filteredMsaFile = "$otuDir/filtered.msa.fasta";
my $filteredCountsFile = "$otuDir/filtered.counts.txt";
my $filteredTaxFile = "$otuDir/filtered.tax.txt";
$logfile = "$otuDir/filered.CONTAM_FILTER.done";
$cmd = "itaggerContamFilter.pl --config $configFile --countsin $countsFile --countsout $filteredCountsFile --msain $msaFile --msaout $filteredMsaFile --taxin $taxFile --taxout $filteredTaxFile --log $logfile";
print $cmd, "\n";
system($cmd) == 0 or die("ERROR: $!\n");

# QIIME
my $filteredTreeFile = "$otuDir/filtered.tree";
my $filteredBiomFile = "$otuDir/filtered.biom";
my $filteredQiimeFile = "$otuDir/qiime.map";
my $filteredQiimeDir = "$otuDir/core_diversity_analyses";
$logfile = "$otuDir/DIVERSITY.done";
if ( -e $logfile and -d $filteredQiimeDir )
{
	warn("QIIME folder exists, not overwriting\n");
} else
{
	$cmd = "itaggerDiversity.pl --config $configFile --counts $filteredCountsFile --tax $filteredTaxFile --msa $filteredMsaFile --tree $filteredTreeFile --biom $filteredBiomFile --map $filteredQiimeFile --dir $filteredQiimeDir --log $logfile";
	print $cmd, "\n";
	system($cmd) == 0 or die("ERROR: $!\n");
}

exit;
__END__
