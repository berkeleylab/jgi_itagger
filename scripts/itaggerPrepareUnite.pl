#!/usr/bin/env perl

=pod

=head1 NAME

itaggerPrepareUnite.pl

=head1 DESCRIPTION

Prepare UNITE database for UTAX.

=head1 OPTIONS

=over 5

=item --in C<file>

UNITE input file; use the larger file which includes singletons, and reps, not just refs.

=item --out C<folder>

Output folder.

=item --threads C<int>

Number of threads to use (optional; default=all).

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright (c) 2016 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=cut

use strict;
use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use constant { FALSE => 0, TRUE => 1 };
use Env qw(TMPDIR USEARCH);
use Getopt::Long;
use Pod::Usage;
use File::Spec qw(rel2abs);
#use File::Path qw(make_path remove_tree);
#use Sys::Hostname;
#use File::Basename;
#use File::Path qw(make_path);
use File::Which;
use iTagger::FastaDb;
use iTagger::Node;

our $VERSION = '2.2';
my $start = time;

# VALIDATE ENV
which('hmmscan') or die("hmmscan executable not found in \$PATH\n");
which('ITSx') or die("ITSx executable not found in \$PATH\n");

# USEARCH
my $usearch;
if ( $USEARCH ) { $usearch = $USEARCH }
elsif ( $usearch = which('usearch64') ) { 1 }
elsif ( $usearch = which('usearch') ) { 1 }
elsif ( $usearch = which('usearch32') ) { 1 }
else { die("usearch executable not found in \$USEARCH or \$PATH\n") }
chomp $usearch;
-f $usearch or die("usearch executable not found: $usearch\n");
my $usearchVersion = `usearch --version`;
chomp $usearchVersion;

# OPTIONS
my $infile;
my $otherFile;
my $outdir;
my $threads;
my $help;
my $man;
GetOptions(
    'infile=s' => \$infile,
	'non=s' => \$otherFile,
    'outdir=s' => \$outdir,
	'threads=i' => \$threads,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
$infile or die("--in file required\n");
$outdir or die("--out dir required\n");
unless ( $threads )
{
	$threads = `nproc` + 0;
	unless ( $threads )
	{
		warn("WARNING: nproc failed; defaulting to threads=1\n");
		$threads = 1;
	}
}
$infile = File::Spec->rel2abs($infile);
$infile =~ /^.+\/sh_general_release_s_(\d{2}\.\d{2}\.\d{4})\.fasta$/ or die("Invalid input file; expected filename sh_general_release_s_DD.MM.YYYY.fasta\n");
my $release = $1;
$outdir = File::Spec->rel2abs($outdir);
-d $outdir or mkdir($outdir) or die("Unable to mkdir $outdir: $!\n");

# FILES
my $hiQualSeqOutfile = "$outdir/itag.unite.ITS.$release.highScoringAmpliconsNR.fasta";
my $lowQualSeqOutfile = "$outdir/itag.unite.ITS.$release.lowScoringAmplicons.fasta";
my $confOutfile="$outdir/itag.unite.ITS.$release.utax.conf.txt";
my $reportOutfile="$outdir/itag.unite.ITS.$release.utax.report.txt";
my $udbOutfile="$outdir/itag.unite.ITS.$release.utax.udb";

# reformat the FASTA header and ensure all node names are unique
our $root = $iTagger::Node::root;
preprocessUnite($root, $infile, $hiQualSeqOutfile, $lowQualSeqOutfile);

# UTAX
generateUtaxFiles($usearch, $hiQualSeqOutfile, $confOutfile, $reportOutfile, $udbOutfile);
exit;

sub signal_handler
{
    warn("INTERRUPTED!\n");
    die($!);
}

sub preprocessUnite
{
	my ($root, $infile, $highQualOutfile, $lowQualOutfile) = @_;

	my $counter = 0; # for making node names unique
	my $id;
	my $name;
	my $tax;
	my @tax;
	my $rank;
	my $seq;
	my $isHighQual;
	my %incertae;
	my $unknown;
	my $db = new iTagger::FastaDb($infile);
	open(my $lo, '>', $lowQualOutfile) or die($!);
	open(my $hi, '>', $highQualOutfile) or die($!);
	while ( my $rec = $db->next_seq )
	{
		$rec->header =~ /^>[^|]+\|\w+\|(SH.+)\|[\w_]+\|(k__.+)$/ or die("Invalid header: ".$rec->header);
		$id = $1;
		@tax = map { s/__/:/; $_ } split(/;/, $2);
		if ( @tax != 7 or $tax[0] !~ /^k:/ or $tax[1] !~ /^p:/ or $tax[2] !~ /^c:/ or $tax[3] !~ /^o:/ or $tax[4] !~ /^f:/ or $tax[5] !~ /^g:/ or $tax[6] !~ /^s:/ )
		{
			warn("Filtering $id for invalid tax: @tax\n");
			next;
		}

		# DISCARD LOW-QUALITY FUNGAL SEQUENCES WITH AMBIGUOUS NT CODES
		if ( $tax[0] eq 'k:Fungi' and $rec->seq =~ /[NRYSWKMBDHV]/ )
		{
			print $lo $rec->output;
			next;
		}

		# IDENTIFY TAXONOMIES WITH UNCERTAIN CLASSIFICATION
		my $fgs = 0;
		$unknown = 0;
		for ( my $i=0; $i<=6; $i++ )
		{
			if ( $i == 6 and $tax[$i] =~ /^s:.+_sp$/ )
			{
				# IF THE SPECIES ISN'T NAMED THEN MAKE UNIQUE BUT DON'T CONSIDER UNCERTAIN
				$tax[6] = 's:Unknown_'.++$counter;
			}
			elsif ( $tax[$i] =~ /^(\w):(Incertae|unknown|unidentified|unclassified)/i or $tax[$i] =~ /^(\w):$/ )
			{
				# LINEAGE IS UNCERTAIN; MUST MAKE THESE NODE NAMES UNIQUE LATER
				$tax[$i] = $1;
				++$unknown;
			}
			elsif ( $i >3 )
			{
				# FAMILY/GENUS/SPECIES IS KNOWN
				++$fgs;
			}
		}

		# IF FUNGAL, FILTER UNLESS FAMILY, GENUS, OR SPECIES IS KNOWN
		if ( $tax[0] eq 'k:Fungi' and $fgs == 0 )
		{
			print $lo $rec->output;
			next;
		}

		# OUTPUT UNLESS UNCERTAIN CLASSIFICATION
		$tax = join(',', @tax);
		if ( $unknown )
		{
			# SAVE FOR LATER
			$rec->id($id);
			$incertae{$tax} = $rec;
		} else
		{
			# OUTPUT
			$rec->id("$id;tax=$tax");
			print $hi $rec->output;
		}
	}
	close($lo);

	# FOR UNCERTAIN CLASSIFICATIONS, MAKE PLACEHOLDER NAMES UNIQUE
	my $match;
	my $new;
	foreach my $tax ( sort keys %incertae )
	{
		my $rec = $incertae{$tax};
		$id = $rec->id;

		# SEE IF MATCHES PREVIOUSLY MADE PLACEHOLDER NODES
		$match and $tax =~ s/$match/$new/;

		# CREATE NODE NODE
		my @tax = split(/,/, $tax);
		my @tax1; # DEFINED NAMES, PRECEEDING UNKNOWN
		my @tax2; # UNDEFINED NAMES
		my @tax3; # DEFINED NAMES, FOLLOWING UNKNOWN
		my $state = 0;
		while ( $name = shift @tax )
		{
			if ( $name =~ /^\w$/ )
			{
				# FIRST UNNAMED NODE
				push @tax2, $name;
				while ( $name = shift @tax )
				{
					if ( $name =~ /^\w$/ )
					{
						# ANOTHER UNNAMED NODE
						push @tax2, $name;
					} else
					{
						# FIRST NAMED NODE FOLLOWING UNNAMED NODE(S)
						push @tax3, $name;
						while ( $name = shift @tax )
						{
							if ( $name =~ /^\w+$/ )
							{
								# ANOTHER UNNAMED NODE; JUST RENAME IT HERE
								warn("Tax has second string on unnamed nodes: $id\n\t$tax\n");
								push @tax3, "$name:Unknown_".++$counter;
							} else
							{
								push @tax3, $name;
							}
						}
					}
				}
			} else
			{
				push @tax1, $name;
			}
		}
		if ( @tax2 )
		{
			$match = join(',', $tax1[$#tax1], @tax2, $tax3[0]);
			for ( my $i=0; $i<=$#tax2; $i++ )
			{
				$tax2[$i] .= ':Unknown_'.++$counter;
			}
			$new = join(',', $tax1[$#tax1], @tax2, $tax3[0]);
		} # else was fixed by previously defined substitution
		$rec->id("$id;tax=".join(',', @tax1, @tax2, @tax3));
		print $hi $rec->output;
	}
	close($hi);
}


sub generateUtaxFiles
{
	my ($infile, $confOutfile, $reportOutfile, $udbOutfile) = @_;
	$infile or die("FASTA file of high-quality reference amplicons required\n");

	# generate utaxconf training file
	my @cmd = qq($usearch -utax_train $infile -taxconfsout $confOutfile -report $reportOutfile -utax_splitlevels NVdpcofg -utax_trainlevels dpcofg);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");

	# generate udb file
	@cmd = qq($usearch -makeudb_utax $infile -taxconfsin $confOutfile -output $udbOutfile);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");
}

__END__
