#!/usr/bin/env perl

=pod

=head1 NAME

itaggerContamFilter.pl

=head1 DESCRIPTION

Filtering of contaminant sequences (e.g. chloroplast).

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration file in C<ini> format.

=item --countsin C<file>

Counts table infile.

=item --taxin C<file>

Taxonomy infile.

=item --msain C<file>

Cluster consensus multiple sequence alignment FASTA infile.

=item --countsout C<file>

Counts table outfile.

=item --taxout C<file>

Taxonomy outfile.

=item --msaout C<file>

Cluster consensus multiple sequence alignment FASTA outfile.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

This pipeline wraps USEARCH; refer to http://drive5.com for documentation, license, and copyright         
information.

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use iTagger::FastaDb;
use iTagger::FastaDb;
use Config::Simple;
use JSON;
use File::Slurp;

our $VERSION = '3.0';
our %log = ();
our $json = new JSON;

# OPTIONS
my $outdir;
my $configFile;
my $msaInfile;
my $countInfile;
my $taxInfile;
my $logfile;
my $verbose;
my $help;
my $man;
GetOptions(
    'config=s' => \$configFile,
	'countsin|countin=s' => \$countInfile,
	'msain=s' => \$msaInfile,
	'taxin=s' => \$taxInfile,
	'countsout|countout=s' => \$countOutfile,
	'msaout=s' => \$msaOutfile,
	'taxout=s' => \$taxOutfile,
	'log=s' => \$logfile,
	'verbose' => \$verbose,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$configFile or die("--config file required\n");
$logfile or die("--log file required\n");
$countInfile or die("--countin file required\n");
$taxInfile or die("--taxin file required\n");
$msaInfile or die("--msain file required\n");
$countOutfile or die("--countout file required\n");
$taxOutfile or die("--taxout file required\n");
$msaOutfile or die("--msaout file required\n");

# LOG
$logfile =~ s/\.done$//;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
-e $passLogFile and die("Operation tagged as done; not overwriting\n");
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");

# FILTER
eval
{
	filterOtus($config, $countInfile, $taxInfile, $msaInfile, $countOutfile, $taxOutfile, $msaOutfile, \%log);
};
$@ and fail("FILTER CONTAMINANTS FAILED: $@\n");

# DONE
write_file($passLogFile, $json->pretty->encode(\%log));
exit;

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler
{
	fail("INTERRUPTED: $!");
	die($!);
}

# Discard contaminant OTUs (i.e. using tax filter regexes) and remove samples with no reads.  Single thread, usearch not required.
sub filterOtus
{
	my ($config, $countInfile, $taxInfile, $fastaIn, $countOutfile, $taxOutfile, $fastaOut, $log) = @_;
	my $keepRegex = $config->param('CONTAM_FILTER.KEEP');
	my $discardRegex = $config->param('CONTAM_FILTER.DISCARD');

	# READ TAX FILE
	my %tax; # otu => tax
	my %chloroplast;
	my %mitochondria;
	open(my $in, '<', $taxInfile) or die($!);
	open(my $out, '>', $taxOutfile) or die($!);
	while (<$in>)
	{
		chomp;
		my ($id, $full, $brief) = split(/\t/);
		if ( $full =~ /chloroplast/i )
		{
			$chloroplast{$id} = undef;
		}
		elsif ( $full =~ /mitochondria/i )
		{
			$mitochondria{$id} = undef;
		}
		elsif ( $keepRegex and $full !~ /$keepRegex/i )
		{
			1;
		}
		elsif ( $discardRegex and $full =~ /$discardRegex/i )
		{
			1;
		} 
		else
		{
			$tax{$id} = undef;
			print $out join("\t", $id, $full, $brief), "\n";
		}
	}
	close($in);
	close($out);

	# FILTER FASTA
	my $db = iTagger::FastaDb->new($fastaIn);
	open($out, '>', $fastaOut) or die($!);
	while ( my $rec = $db->next_seq )
	{
		exists($tax{$rec->id}) and print $out $rec->output;
	}
	close($out);

	# FILTER CONTAMINANT OTUS FROM COUNTS TABLE AND COUNT READS
	my $numReads = 0;
	my $numChloroplastReads = 0;
	my $numMitochondriaReads = 0;
	my $numContamReads = 0;
	open($in, '<', $countInfile) or die($!);
	open($out, '>', $countOutfile) or die($!);
	my $hdr = <$in>;
	print $out $hdr;
	while (<$in>)
	{
		my ($otuId, @counts) = split(/\t/);
		my $n = 0;
		$n += $_ foreach @counts;
		$numReads += $n;
		if ( exists($tax{$otuId}) )
		{
			print $out $_;
		} elsif( exists($chloroplast{$otuId}) )
		{
			$numChloroplastReads += $n;
		} elsif ( exists($mitochondria{$otuId}) )
		{
			$numMitochondriaReads += $n;
		} else
		{
			$numContamReads += $n;
		}
	}
	close($in);
	close($out);
	$log->{NUM_READS} = $numReads;
	my $pctChloroplastReads = int($numChloroplastReads/$numReads*100+0.5);
	$log->{NUM_CHLOROPLAST_READS} = $numChloroplastReads;
	$log->{PCT_CHLOROPLAST_READS} = $pctChloroplastReads;
	my $pctMitochondriaReads = int($numMitochondriaReads/$numReads*100+0.5);
	$log->{NUM_MITOCHONDRIA_READS} = $numMitochondriaReads;
	$log->{PCT_MITOCHONDRIA_READS} = $pctMitochondriaReads;
	my $pctContamReads = int($numContamReads/$numReads*100+0.5);
	$log->{NUM_CONTAM_READS} = $numContamReads;
	$log->{PCT_CONTAM_READS} = $pctContamReads;
	my $numFilteredReads = $numChloroplastReads + $numMitochondriaReads + $numContamReads;
	my $pctFilteredReads = int($numFilteredReads/$numReads*100+0.5);
	$log->{NUM_FILTERED_READS} = $numFilteredReads;
	$log->{PCT_FILTERED_READS} = $pctFilteredReads;
}

# Remove columns from an array.
sub _removeColumns
{
	my ($table, $columns) = @_;
	@$columns or return;
	my @columns = sort { $b<=>$a } @$columns; # sort desc array indices
	foreach my $col (@columns)
	{
		foreach my $row (@$table) { splice($row, $col, 1) }
	}
}

__END__
