#!/usr/bin/env perl

=pod

=head1 NAME

itaggerPrepareSilva.pl

=head1 SYNOPSIS

itaggerPrepareSilva.pl --in ./mirror/silva_123 --primers ./itagger/db/SSU_V4-V5.fasta --out ./itagger/db/silva_123

=head1 DESCRIPTION

Prepare the Silva rRNA reference database for itagger.

=head1 OPTIONS

=over 5

=item --in C<path>

Silva input folder.

=item --primers C<path>

Primers in FASTA format.

=item --out C<path>

Output folder.

=item --diff C<int>

Maximum differences (as percent) in electronic PCR amplification step (optional; default=10).

=item --gene C<string>

Either 'ssu' or 'lsu' for small and large ribosomal subunit, respectively.  Optional; default=ssu.

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT

Copyright 2016 by the United States Department of Energy Joint Genome Institute

Use freely under the same license as perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=head1 CLASS NAME

Node

=head1 METHODS

=over 5

=cut

use strict;
use warnings;
use Env qw(TMPDIR USEARCH);
#use sigtrap qw/handler signal_handler normal-signals/;

package Node;

use constant
{
	FALSE => 0,
	TRUE => 1,

	# NODES ARE ARRAYREFS, NOT HASHREFS
	ID => 0, # string; Silva taxon Ids plus iTagger (placeholder) taxon Ids
	NAME => 1, # string; taxonomic name
	RANK => 2, # integer; index on @rankNames
	PARENT => 3, # Node object ref
	CHILDREN => 4, # arrayref
	ACCESSIONS => 5, # arrayref

	# DEFAULTS
	DEFAULT_MIN_SEQ_SCORE => 95,
	DEFAULT_MIN_ALIGN_SCORE => 95,
	DEFAULT_MIN_PINTAIL_SCORE => 95,

	# ACCESSIONS STRUCT
	OWNER_NODE => 0,
	AMPLICON => 1,
	SEQ_SCORE => 2,
	ALIGN_SCORE => 3,
	PINTAIL_SCORE => 4,
};

# CONSTANT REFERENCE DATA
our @rankNames =
(
	'rootrank', # not in NCBI
	'domain', # not in NCBI
	'major_clade', # not in NCBI
	'superkingdom',
	'kingdom',
	'subkingdom',
	'infrakingdom', # not in NCBI
	'superphylum',
	'phylum',
	'subphylum',
	'infraphylum', # not in NCBI
	'superclass',
	'class',
	'subclass',
	'infraclass',
	'superorder',
	'order',
	'suborder',
	'superfamily',
	'family',
	'subfamily',
	'genus',
	'species'
); # ordered list of valid taxonomic ranks.
our %rankIndices = ( 'No rank' => undef );
our @allNames;
for (my $i=0; $i<@rankNames; $i++)
{
	$rankIndices{$rankNames[$i]} = $i;
	$allNames[$i] = {};
}
our @mainRanks = (1, 8, 12, 16, 19, 21);
our %mainRankCodes  = ( 1 => 'd', 8 => 'p', 12 => 'c', 16 => 'o', 19 => 'f', 21 => 'g' );
our %mainRankLevel  = ( 1 => 0, 8 => 1, 12 => 2, 16 => 3, 19 => 4, 21 => 5 );

# CLASS/GLOBAL VARIABLES
our $tmpNodeId = 1_000_000; # we create placeholder nodes for all missing major taxonomic levels
our %accessions; # { accession => [ node ref, amplicon sequence, seq score, align score, pintail score ] }
our @mainRankNames; # [ { name => undef }, ... ]

=item new

Create taxonomic node.

=cut

sub new
{
	my ($class, $taxId, $name, $rank, $parent, $children) = @_;
	if ( !defined($taxId) )
	{
		$taxId = ++$tmpNodeId;
	}
	if ( $rank )
	{
		# OPTIONAL RANK PARAMETER MAY EITHER BE A NAME OR INDEX
		if ( $rank =~ /^\d+/ )
		{
			# ARG IS INTEGER RANK INDEXT
			$rank < @rankNames or die("FATAL: Invalid rank index, $rank, for $name ($taxId)\n");
		} else
		{
			# ARG IS STRING RANK NAME; CONVERT TO RANK INDEX
			if ( exists($rankIndices{$rank}) )
			{
				$rank = $rankIndices{$rank};
			} else
			{
				die("FATAL: Invalid rank name, $rank, for $name ($taxId)\n");
			}
		}
	}
	else
	{
		warn("WARNING: Rank not defined for $name (taxon Id: $taxId)\n");
		$rank = undef;
	}
	if ( !defined($name) )
	{
		if ( $rank )
		{
			$name = 'Unknown '.ucfirst($rankNames[$rank]);
		} else
		{
			die("FATAL: Neither rank or name defined for taxon Id: $taxId\n");
		}
	}
	if ( $taxId == 0 )
	{
		defined($parent) and die("Root should not have a parent\n"); 
	} else
	{
		defined($parent) or die("Parent required\n");
	}
	if ( defined($children) )
	{
		ref($children) eq 'ARRAY' or die("Children must be an arrayref");
	} else
	{
		$children = [];
	}
	my $node = [ $taxId, $name, $rank, $parent, $children, [] ];
	bless $node, $class;
	return $node;
}

=back

=head2 Methods for basic node attributes

=over 5

=item id

Returns the taxon ID for the node.

=cut

sub id { return shift->[ID] }

=item name

Returns the taxonomic name of the node.

=cut

sub name { return shift->[NAME] }

=item strictName

Returns the taxonomic name of the node, with any non-word characters replaced by underscores.

=cut

sub strictName
{
	my $node = shift;
	my $name = $node->name;
	$name =~ s/\W/_/g;
	return $name;
}

=item rank

Returns the integer rank of the node.

=cut

sub rank
{
	my ($node, $rank) = @_;
	if ( $rank )
	{
		$rank =~ /^\d+$/ or die("FATAL: Rank must be an integer; got: $rank");
		$rank < @rankNames or die("FATAL: Invalid rank: $rank");
		return $node->[RANK] = $rank;
	}
	return $node->[RANK];
}

=item rankName

Returns the rank name (string) of the node.

=cut

sub rankName
{
	my ($node, $rankName) = @_;
	if ( $rankName )
	{
		$rankName =~ /^\d+$/ and die("Rank name must be a string; did you mean to use &rank?\n");
		exists($rankIndices{$rankName}) or die("Invalid rank name: $rankName\n");
		$node->[RANK] = $rankIndices{$rankName};
	}
	return defined($node->rank) ? $rankNames[$node->rank] : 'No rank';
}

=back

=head2 Methods for relationships/links

=over 5

=item parent

Returns the parent (node objref) of this node; undef if root.

=cut

sub parent
{
	my ($node, $newParent) = @_;
	if ( defined($newParent) )
	{
		$newParent->isa('Node') or die("Parent must be a valid Node\n");
		$node->[PARENT] = $newParent;
	}
	return $node->[PARENT];
}

=item parentId

Returns parent id; -1 if root

=cut

sub parentId
{
	my $node = shift;
	return defined($node->parent)  ? $node->parent->id : -1;
}

=item children

Returns a list of this node's children (node objrefs).

=cut

sub children
{
	my $node = shift;
	my @children = @{$node->[CHILDREN]}; # deep copy
	return @children;
}

=item addChild

Add a child node to this node.

=cut

sub addChild
{
	my ($node, $child) = @_;
	defined($child) or die("Child required");
	$child->isa('Node') or die("Node object expected\n");
	push @{$node->[CHILDREN]}, $child;
}

=item setChildren

Replace node's current list of children with a new arrayref.

=cut

sub setChildren
{
	my ($node, $ar) = @_;
	ref($ar) eq 'ARRAY' or $ar = [];
	if ( scalar(@$ar) )
	{
		foreach my $child (@$ar)
		{
			$child->isa('Node') or die("Child is not a node\n");
		}
	}
	$node->[CHILDREN] = $ar;
}

=item outputNode

Output node linkage and basic information via depth-first recursion

=cut

sub outputNode
{
	my ($node, $out) = @_;
	defined($out) or die("Output file handle required\n");
	my $rank = $node->rank;
	if ( defined($rank) and exists($mainRankLevel{$rank}) )
	{
		print $out join('*', $node->id, $node->strictName, $node->parentId, $mainRankLevel{$rank}, $node->rankName), "\n";
	}
	$_->outputNode($out) foreach $node->children;
}

=item outputTaxonomy

Output node info and links.

=cut

sub outputTaxonomy
{
	my ($node, $out) = @_;
	defined($out) or die("Output file handle required\n");
	my $rank = $node->rank;
	if ( defined($rank) and exists($mainRankLevel{$rank}) )
	{
		print $out $node->id, "\t", $node->name, "\t", $node->parentId, "\t", join(';', $node->taxonomyIds), "\t", join(';', $node->taxonomy), "\n";
	}
	$_->outputTaxonomy($out) foreach $node->children;
}

=item taxonomy

Returns the taxonomic path of a node.

=cut

sub taxonomy
{
	my $node = shift;
	return defined($node->parent) ? ( $node->parent->taxonomy, $node->name ) : ( $node->name );
}

=item taxonomyIds

Returns a list of the taxon IDs of a node's taxonomic path.

=cut

sub taxonomyIds
{
	my $node = shift;
	return defined($node->parent) ? ( $node->parent->taxonomyIds, $node->id ) : ( $node->id );
}

=item majorTaxonomy

Returns taxonomy of major levels only and remove any non-word characters

=cut

sub majorTaxonomy
{
	my $node = shift;
	my @tax = defined($node->parent) ? $node->parent->majorTaxonomy : ( );
	if ( defined($node->rank) and exists($mainRankCodes{$node->rank}) )
	{
		my $name = $node->strictName;
		my $code = $mainRankCodes{$node->rank};
		if ( $code ) { $name = $code.':'.$name }
		push @tax, $name;
	}
	return @tax;
}

=item domain

Returns domain of a node, if defined; otherwise undef.

=cut

sub domain
{
	my $node = shift;
	defined($node->rank) and $node->rank == 1 and return $node->name;
	defined($node->parent) and return $node->parent->domain;
	return undef;
}

=item unsetInvalidRanks

Invalid ranks (e.g. when child has higher or equal rank as parent) are set to 'no rank'

=cut

sub unsetInvalidRanks
{
	my ($node, $lastRank) = @_;
	my $errors = 0;
	if ( defined($node->rank) and defined($lastRank) )
	{
		if ( $node->rank < $lastRank )
		{
			warn('WARNING: '.$node->name.' ('.$node->id.') RANK '.$node->rankName.' < PARENT RANK '.$rankNames[$lastRank]."; changing to 'No rank'\n");
			$node->rankName('No rank');
			++$errors;
		} elsif ( $node->rank == $lastRank )
		{
			warn('WARNING: '.$node->name.' ('.$node->id.') RANK '.$node->rankName.' == PARENT RANK '.$rankNames[$lastRank]."; changing to 'No rank'\n");
			$node->rankName('No rank');
			++$errors;
		}
	}
	foreach my $child ( $node->children )
	{
		$errors += $child->unsetInvalidRanks($node->rank); 
	}
	return $errors;
}

=item makeNamesUnique

In order to ensure all names are unique, the taxon ID is appended where necessary.

=cut

sub makeNamesUnique
{
	my $node = shift;
	my $rank = $node->rank;
	if ( defined($rank) )
	{
		my $namesHR = $allNames[$rank];
		my $name = $node->name;
		if ( $name =~ /^Unknown/ or $name =~ /Incertae/ or exists($namesHR->{$name}) )
		{
			$node->[NAME] .= ':'.$node->id;
		}
		if ( exists($namesHR->{$node->name}) )
		{
			die("Added taxon ID suffix but name is still not unique: ".$node->name."\n");
		}
		$namesHR->{$node->name} = undef;
	}
	$_->makeNamesUnique foreach $node->children;
}

=item pushSeqsToLeaves

Disallow sequences on internal nodes.  Create leaf nodes, which in the Silva taxonomy are rank 'genus', and move sequences.

=cut

sub pushSeqsToLeaves
{
	my $node = shift;
	my $leafRank = $mainRanks[$#mainRanks];
	defined($node->rank) and $node->rank == $leafRank and return;
	my @accessions = $node->getAccessions;
	if ( @accessions )
	{
		$node->[ACCESSIONS] = [];
		my $newNode = Node->new(undef, undef, $leafRank, $node);
		$node->addChild($newNode);	
		$newNode->[ACCESSIONS] = \@accessions;
		$accessions{$_}->[OWNER_NODE] = $newNode foreach @accessions;
	}
	$_->pushSeqsToLeaves foreach $node->children;
}

=item insertMajorNodes

Traverse taxonomic paths and create placeholder nodes where major taxonomic levels (e.g. class, order, etc.) are missing.

=cut

sub insertMajorNodes
{
	my ($node, $nextMainRankIndex) = @_;
	defined($nextMainRankIndex) or $nextMainRankIndex = 0; # NOTE is index on @mainRanks, not @rankNames!
	return if $nextMainRankIndex > $#mainRanks;
	my $nextMainRankIndex2 = $nextMainRankIndex + 1;
	my $nextRank = $mainRanks[$nextMainRankIndex]; # index on @rankNames
	my @lt;
	my @eq;
	my @gt;
	foreach my $child ( $node->children )
	{
		if ( !defined($child->rank) or $child->rank < $nextRank ) { push @lt, $child }
		elsif ( $child->rank == $nextRank ) { push @eq, $child }
		else { push @gt, $child }
	}
	my @children = ( @lt, @eq );
	$node->setChildren(\@children);
	if ( @gt )
	{
		# INSERT NEW MAJOR RANK NODE
		my $newNode = Node->new(undef, undef, $nextRank, $node, \@gt);
		$node->addChild($newNode);
		$_->parent($newNode) foreach @gt;
		$newNode->insertMajorNodes($nextMainRankIndex2);;
	}
	$_->insertMajorNodes($nextMainRankIndex2) foreach @eq;
	$_->insertMajorNodes($nextMainRankIndex) foreach @lt;
}

=item taxSearch

Search subtree for node specified by taxonomic path; return node if found, undef otherwise.
Note that the insertion of placeholder nodes will change taxonomic paths.

=cut

sub taxSearch
{
	my $node = shift;
	scalar(@_) or return $node;
	my $name = shift;

	foreach my $child ( $node->children )
	{
		$child->name eq $name and return $child->taxSearch(@_);
	}
	return undef;
}
=back

=head2 Methods for managing accessions and quality scores

Each node has a list of accession IDs associated with it.  A class hash ensures the mapping is unique.

=over 5

=item addAccession

Associates an accession with this node.

=cut

sub addAccession
{
	my ($node, $acc) = @_;
	$acc or die("Accession required");
	exists($accessions{$acc}) and die("Accession $acc was already added\n");
	push @{$node->[ACCESSIONS]}, $acc; # add to this node
	$accessions{$acc} = [ $node, undef, undef, undef, undef ]; # update class hash
}

=item getAccessions

Returns list of accession IDs associated with this node.

=cut

sub getAccessions
{
	my $node = shift;
	return @{$node->[ACCESSIONS]};
}

=item outputAccessions

Write nodes accessions of subtree to file handle via depth-first traversal.  Only nodes with accessions are output.

=cut

sub outputAccessions
{
	my ($node, $out) = @_;
	defined($out) or die("Output file handle required\n");
	my @accessions = $node->getAccessions;
	@accessions and print $out $node->id, "\t", join(',', @accessions), "\n";
	$_->outputAccessions($out) foreach $node->children;
}

=item addAccessionScores

Add quality scores to an accession.

=cut

sub addAccessionScores
{
	my ($node, $acc, $seqScore, $alignScore, $pintailScore) = @_;
	defined($acc) or die("Accession required\n");
	exists($accessions{$acc}) or die("Unknown accession: $acc\n");
	$accessions{$acc}->[OWNER_NODE] == $node or die("Accession $acc is not assigned to this node\n");
	defined($seqScore) or die("Sequence score required\n");
	defined($alignScore) or die("Alignment score required\n");
	defined($pintailScore) or die("Pintail score required\n");
	$seqScore eq 'NULL' and $seqScore = undef;
	$alignScore eq 'NULL' and $alignScore = undef;
	$pintailScore eq 'NULL' and $pintailScore = undef;
	$accessions{$acc}->[SEQ_SCORE] = $seqScore;
	$accessions{$acc}->[ALIGN_SCORE] = $alignScore;
	$accessions{$acc}->[PINTAIL_SCORE] = $pintailScore;
}

=item accToNode

Any node object can return another node object given a query accession ID.  Usually this is done using the root.  Returns undef if accession does not exist or has not been assigned to a node.

=cut

sub accToNode
{
	my ($node, $acc) = @_;
	$acc or die("Accession required\n");
	exists($accessions{$acc}) or return undef;
	return $accessions{$acc}->[0]; # may be undef
}

=item isHighQuality

Returns TRUE if sequence has high quality scores; FALSE otherwise.

=cut

sub isHighQuality
{
	my ($node, $acc, $minSeqScore, $minAlignScore, $minPintailScore) = @_;
	exists($accessions{$acc}) or die("Accession not known: $acc\n");
	defined($minSeqScore) or $minSeqScore = DEFAULT_MIN_SEQ_SCORE;
	defined($minAlignScore) or $minAlignScore = DEFAULT_MIN_ALIGN_SCORE;
	defined($minPintailScore) or $minPintailScore = DEFAULT_MIN_PINTAIL_SCORE;
	my ($ownerNode, $seq, $seqScore, $alignScore, $pintailScore) = @{$accessions{$acc}};
	( defined($seqScore) and $seqScore >= $minSeqScore ) or return FALSE;
	( defined($alignScore) and $alignScore >= $minAlignScore ) or return FALSE;
	( defined($pintailScore) and $pintailScore >= $minPintailScore ) or return FALSE;
	return TRUE;
}

=back

=head2 Methods for managing amplicon sequences.

=over 5

=item addSeq

Save amplicon sequence.

=cut

sub addSeq
{
	my ($node, $acc, $seq) = @_;
	$acc or die("Accession required\n");
	$seq or die("Sequence required\n");
	exists($accessions{$acc}) or die("Unknown accession: $acc\n");
	defined($accessions{$acc}->[AMPLICON]) and die("Amplicon sequence already defined for $acc\n");
	push @{$node->[ACCESSIONS]}, $acc;
	$accessions{$acc}->[AMPLICON] = $seq;
}

=item getSeq

Returns the specified sequence; may be undef.

=cut

sub getSeq
{
	my ($node, $acc) = @_;
	exists($accessions{$acc}) or die("Unknown accession: $acc\n");
	return $accessions{$acc}->[AMPLICON];
}

=item outputFasta

Writes FASTA record of all unique sequences associated with this node.

=cut

sub outputFasta
{
	my ($node, $out) = @_;
	defined($out) or die("Output file handle required\n");
	my %amplicons; # nonredundant set
	foreach my $acc ( $node->getAccessions )
	{
		my $seq = $node->getSeq($acc);
		defined($seq) and $amplicons{$seq} = $acc; # not defined for low-qual seqs
	}
	my $nodeInfo = $node->id.';tax='.join(',', $node->majorTaxonomy);
	foreach my $seq ( keys %amplicons )
	{
		my $acc = $amplicons{$seq};
		print $out '>', $acc, ':', $nodeInfo, "\n", reformatSeqForFasta($seq);
	}
	$_->outputFasta($out) foreach $node->children;
}

=item reformatSeqForFasta

Nonmember helper function adds newline characters every 60 nucleotides.

=cut

sub reformatSeqForFasta
{
	my $seq = shift;
	$seq or die("Sequence required");
	my $result = '';
	while ( length($seq) > 60 )
	{
		$result .= substr($seq, 0, 60)."\n";
		$seq = substr($seq, 60);
	}
	if ( $seq )
	{
		$result .= $seq."\n";
	}
	return $result;
}

###############################################################################
# DRIVER #
##########

use Getopt::Long;
use Pod::Usage;
use File::Basename;
use File::Path qw(make_path);
use File::Which;
use Env qw(TMPDIR USEARCH);

our $VERSION = '2.2';
my $start = time;

# USEARCH
my $usearch;
if ( $USEARCH ) { $usearch = $USEARCH }
elsif ( $usearch = which('usearch64') ) { 1 }
elsif ( $usearch = which('usearch') ) { 1 }
elsif ( $usearch = which('usearch32') ) { 1 }
else { die("usearch executable not found in \$USEARCH or \$PATH\n") }
chomp $usearch;
-f $usearch or die("usearch executable not found: $usearch\n");

my $usearchVersion = `usearch --version`;
chomp $usearchVersion;

# OPTIONS
my $gene = 'ssu';
my $indir;
my $outdir;
my $primersInfile;
my $maxDiffsPct = 10;
my $help;
my $man;
GetOptions(
	'gene=s' => \$gene,
    'in=s' => \$indir,
    'out=s' => \$outdir,
	'primers=s' => \$primersInfile,
	'diff=i' => \$maxDiffsPct,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;
die("--in dir required\n") unless $indir;
die("--primers file required\n") unless $primersInfile;
die("--out dir required\n") unless $outdir;
-d $outdir or make_path($outdir) or die("Unable to create output dir, $outdir: $!\n");
$gene =~ /^[ls]su/i or die("--gene must be SSU or LSU\n");
$gene = lc($gene);
my $geneUC=uc($gene);

# OPEN LOG
#my $logFile = "$outdir/log.txt";
#open(my $log, '>', $logFile) or die("Unable to write log file, $logFile: $!\n");

# GET RELEASE VERSION
my $release;
my ($file, $dir) = fileparse($indir, qr/\.[^.]*/);
if ( $file and $file =~ /^\d+$/) { $release = $file }
elsif ( $dir =~ /\/(\d+)\/?$/ ) { $release = $1 }
else { die("Expected Silva files to be in a subdir named after the release version\n") }
print "Processing Silva release version $release\n";

# DEFINE FILENAMES AND VERIFY THEY EXIST
my $seqInfile = "$indir/Exports/SILVA_${release}_${geneUC}Ref_tax_silva_trunc.fasta.gz";
my $nodesInfile = "$indir/Exports/taxonomy/tax_slv_${gene}_${release}.txt";
my $qualInfile = "$indir/Exports/quality/SILVA_${release}_${geneUC}Ref.quality.gz";
-f $seqInfile or die("File not found: $seqInfile\n");
-f $nodesInfile or die("File not found: $nodesInfile\n");
-f $primersInfile or die("File not found: $primersInfile\n");
-f $qualInfile or die("File not found: $qualInfile\n");
my $nodesOutfile = "$outdir/itag.silva.$gene.$release.nodes.txt";
my $lowScoreSeqOutfile = "$outdir/itag.silva.$gene.$release.lowScoringAmplicons.fasta";
my $highScoreSeqOutfile = "$outdir/itag.silva.$gene.$release.highScoringAmpliconsNR.fasta";
my $unamplifiedOutfile = "$outdir/itag.silva.$gene.$release.unamplified.fasta";
my $taxOutfile = "$outdir/itag.silva.$gene.$release.tax.txt";
my $accessionsOutfile = "$outdir/itag.silva.$gene.$release.acc.txt";
my $tmpfile = "$outdir/itag.silva.$gene.$release.tmp.fasta";
#my $duplicateAmpliconsOutfile = "$outdir/itag.silva.$gene.$release.duplicateAmplicons.txt";
my $utaxConfOutfile = "$outdir/itag.silva.$gene.$release.utax.conf.txt";
my $utaxReportOutfile = "$outdir/itag.silva.$gene.$release.utax.report.txt";
my $utaxUdbOutfile = "$outdir/itag.silva.$gene.$release.utax.udb";

print "BEGIN processing Silva $gene\n";

print "Loading internal nodes of Silva taxonomy: $nodesInfile\n";
my $root = loadSilvaNodesFile($nodesInfile);

print "Reformatting FASTA file and loading accession IDs: $seqInfile\n";
preprocessSilvaFasta($root, $seqInfile, $tmpfile);

print "Loading quality scores: $qualInfile\n";
loadSilvaQualityScoresFile($root, $qualInfile);

print "In silico PCR using $usearchVersion: $primersInfile\n";
ePcr($usearch, $root, $tmpfile, $lowScoreSeqOutfile, $primersInfile, $unamplifiedOutfile, $maxDiffsPct);
unlink($tmpfile) or warn("ERROR unlinking tmpfile, $tmpfile: $!\n");

print "Changing invalid ranks to 'no rank'\n";
while ( $root->unsetInvalidRanks ) { 1 }

print "Moving sequences to leaf nodes\n";
$root->pushSeqsToLeaves;

print "Inserting placeholder nodes for missing major taxonomic levels\n";
$root->insertMajorNodes;

print "Make names unique\n";
$root->makeNamesUnique;

print "Writing node info file: $nodesOutfile\n";
open(my $out, '>', $nodesOutfile) or die($!);
$root->outputNode($out);
close($out);

print "Writing taxonomy file: $taxOutfile\n";
open($out, '>', $taxOutfile) or die($!);
$root->outputTaxonomy($out);
close($out);

print "Writing node:accessions table: $accessionsOutfile\n";
open($out, '>', $accessionsOutfile) or die($!);
$root->outputAccessions($out);
close($out);

print "Writing selected references seqs: $highScoreSeqOutfile\n";
open($out, '>', $highScoreSeqOutfile) or die($!);
$root->outputFasta($out);
close($out);

#print "Checking for amplicon sequences assigned to multiple nodes\n";
#checkForDuplicateAmplicons($usearch, $root, $highScoreSeqOutfile, $duplicateAmpliconsOutfile);

print "Generating UTAX files using $usearchVersion: $utaxReportOutfile\n";
generateUtaxFiles($usearch, $highScoreSeqOutfile, $utaxConfOutfile, $utaxReportOutfile, $utaxUdbOutfile);

my $min = int((time - $start)/60 + 0.5);
print "Done after $min min\n";
exit;

=item loadSilvaNodesFile

Init nodes from Silva nodes file.

=cut

sub loadSilvaNodesFile
{
	my $infile = shift;
	$infile or die("Input file required");
	my $root = Node->new(0, 'Root', 'rootrank');
	open(my $in, '<', $infile) or die("Unable to open infile, $infile: $!\n");
	while (<$in>)
	{
		chomp;
		my ($tax, $taxId, $rankName) = split(/\t/);
		$tax =~ s/;$//; # remove trailing ';'
		my @tax = split(/;/, $tax);
		my $name = pop @tax;
		my $parent = $root->taxSearch(@tax) or die("FATAL: Parent not found: ".join(';', @tax)."\n");
		my $node = Node->new($taxId, $name, $rankName, $parent);
		$parent->addChild($node);
	}
	close($in);
	return $root;
}

=item preprocessSilvaFasta

Uncompress FASTA infile, convert RNA to DNA, save accession ID, and write DNA FASTA tmpfile.

=cut

sub preprocessSilvaFasta
{
	my ($root, $infile, $outfile) = @_;
	$infile or die("Input sequence file required\n");
	$outfile or die("Output sequence file required\n");
	my $numInput = 0;
	my $acc;
	my $tax;
	my @tax;
	my $name;
	my $node;
	my $ok = 0;
	open(my $in, "gunzip -c $infile|") or die($!);
	open(my $out, '>', $outfile) or die($!);
	while ( my $line = <$in>)
	{
		if ( $line =~ /^>(\S+) (.+)$/ )
		{
			++$numInput;
			$acc = $1;
			$tax = $2;
			exists($accessions{$acc}) and die("Accession $acc was already added\n");
			@tax = split(/;/, $tax);
			my $species = pop @tax; # not used
			$node = $root->taxSearch(@tax);
			if ( defined($node) )
			{
				$node->addAccession($acc);
				$ok = 1;
			} else
			{
				warn("Node not found: ".join(';', @tax)."; skipping\n");
			}
		} else
		{
			$line =~ tr/Uu/Tt/;
		}
		if ( $ok )
		{
			print $out $line;
		}
	}
	close($in);
	close($out);
	warn("INFO: Input DB contains $numInput sequences\n");
}

=item loadSilvaQualityScoresFile

Init accessions and quality scores from Silva quality file.

=cut

sub loadSilvaQualityScoresFile
{
	my ($root, $infile) = @_;
	open(my $in, "gunzip -c $infile|") or die($!);
	my $hdr = <$in>;
	$hdr or die("Quality file is empty\n");
	my $numFound = 0;
	my $numNotFound = 0;
	while (<$in>)
	{
		chomp;
		my ($srcAcc, $st, $sp, $len, $src, $seqScore, $ambig, $homo, $vect, $alignScore, $bpScore, $bpAligned, $pintailScore) = split(/\t/);
		my $acc = join('.', $srcAcc, $st, $sp);
		my $node = $root->accToNode($acc);
		if ( defined($node) )
		{
			++$numFound;
			$node->addAccessionScores($acc, $seqScore, $alignScore, $pintailScore);
		} else
		{
			++$numNotFound;
		}
	}
	close($in);
	warn("INFO: $numFound accession quality scores were saved; $numNotFound accessions were not found\n");
}

=item ePcr

Electronic/in silico PCR using USEARCH.  The highest scoring sequences are loaded while the remainder are output to the low quality sequence outfile.  maxdiffs should be lenient enough to amplify all branches (i.e. Eukaryota too).

=cut

sub ePcr
{
	my ($usearch, $root, $seqInfile, $seqOutfile, $primersInfile, $unamplifiedOutfile, $maxDiffsPct) = @_;
	$seqInfile or die("Input sequence file required\n");
	$seqOutfile or die("Output sequence file required\n");
	-f $seqInfile or die("Infile not found: $seqInfile\n");
	$maxDiffsPct or die("Percent maximum differences parameter required\n");
	my $tmpfile = "$seqOutfile.pcrout.txt"; # usearch e-pcr outfile

	# CALC MAX DIFFS
	my $primerLen = 0;
	my $fwd;
	my $rev;
	open(my $in, '<', $primersInfile) or die($!);
	while (<$in>)
	{
		chomp;
		if (/^>(\S+)/)
		{
			if ( ! defined($fwd) ) { $fwd = $1 }
			elsif ( ! defined($rev) ) { $rev = $1 }
			else { die("Expected exactly two primer sequences\n") }
		} else
		{
			$primerLen += length($_);
		}
	}
	close($in);
	unless ( defined($fwd) and defined($rev) ) { die("Expected exactly two primer sequences\n") }
	my $maxDiffs = int($maxDiffsPct/100 * $primerLen / 2 + 0.5);
	warn("INFO: Forward primer: $fwd; Reverse primer: $rev; total primer len = $primerLen; max diffs = $maxDiffsPct% or $maxDiffs bp\n");

	# RUN USEARCH
	my @cmd = qq($usearch -search_pcr $seqInfile -db $primersInfile -pcrout $tmpfile -strand both -maxdiffs $maxDiffs);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");

	# SORT RESULTS SO ALL HITS FOR AN ACCESSION APPEAR CONSECUTIVELY
	@cmd = qq(sort -o $tmpfile -i $tmpfile);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");

	# PARSE RESULTS, LOAD HIGH-QUALITY AMPLICON SEQUENCES, OUTPUT FILTERED LOW-QUALITY SEQUENCES
	my $numAmplified = 0;
	my $numHighQuality = 0;
	my %numHighQualityDomain;
	my $prevAcc = '';
	my $prevMis;
	my $prevLen;
	my $prevSeq;
	my $node;
	my %accAmplified;
	open($in, '<', $tmpfile) or die($!);
	open($out, '>', $seqOutfile) or die($!); # seqs with sub-threshold Silva quality scores
	while (<$in>)
	{
		chomp;
		my ($acc, $st, $sp, $srcLen, $primer1, $strand1, $align1, $primer2, $strand2, $align2, $len, $seq) = split(/\t/);
		if ( $primer1 eq $fwd and $strand1 eq '+' and $primer2 eq $rev and $strand2 eq '-' ) { 1 }
		elsif ( $primer2 eq $fwd and $strand2 eq '+' and $primer1 eq $rev and $strand1 eq '-' ) { 1 }
		else { next }
		my $align = $align1.$align2;
		$align =~ s/\.//g;
		my $mis = length($align);
		if ( $acc eq $prevAcc )
		{
			if ( $mis > $prevMis or ( $mis == $prevMis and $len <= $prevLen ) )
			{
				next;
			}
		} elsif ( $prevAcc )
		{
			# SAVE PREVIOUS
			++$numAmplified;
			$accAmplified{$prevAcc} = TRUE;
			$node = $root->accToNode($prevAcc);
			if ( defined($node) )
			{
				if ( $node->isHighQuality($prevAcc) )
				{
					++$numHighQuality;
					$node->addSeq($prevAcc, $prevSeq);
					my $d = $node->domain;
					if ( exists($numHighQualityDomain{$d}) ) { $numHighQualityDomain{$d} += 1 }
					else { $numHighQualityDomain{$d} = 1 }
				} else
				{
					print $out '>', $prevAcc, ':', $node->id, ' ', join(';', $node->majorTaxonomy), "\n", reformatSeqForFasta($prevSeq);
				}
			} else
			{
				warn("Node not found for acc $prevAcc; filtering\n");
			}
		}
		$prevAcc = $acc;
		$prevMis = $mis;
		$prevLen = $len;
		$prevSeq = $seq;
	}
	# SAVE PREVIOUS
	++$numAmplified;
	$accAmplified{$prevAcc} = TRUE;
	$node = $root->accToNode($prevAcc) or die("Node not found for acc $prevAcc\n");
	if ( $node->isHighQuality($prevAcc) )
	{
		++$numHighQuality;
		$node->addSeq($prevAcc, $prevSeq);
		my $d = $node->domain;
		if ( exists($numHighQualityDomain{$d}) ) { $numHighQualityDomain{$d} += 1 }
		else { $numHighQualityDomain{$d} = 1 }
	} else
	{
		print $out '>', $prevAcc, ':', $node->id, ' ', join(';', $node->majorTaxonomy), "\n", reformatSeqForFasta($prevSeq);
	}
	close($in);
	close($out);
	$numAmplified or die("FATAL: no sequences amplified\n");

	# SUMMARY
	print "Number of high quality subsequences per domain:\n";
	foreach my $domain ( sort { $a cmp $b } keys %numHighQualityDomain )
	{
		print "\t$domain = $numHighQualityDomain{$domain}\n";
	}

	# CREATE UNAMPLIFIED FILE
	my $outputFlag = FALSE;
	my $numUnamplified = 0;
	open($in, '<', $seqInfile) or die($!);
	open($out, '>', $unamplifiedOutfile) or die($!);
	while (<$in>)
	{
		if (/^>(\S+)/)
		{
			if ( exists($accAmplified{$1}) )
			{
				$outputFlag = FALSE;
			} else
			{
				++$numUnamplified;
				$outputFlag = TRUE;
			}
		}
		$outputFlag and print $out $_;
	}
	close($in);
	close($out);
	unlink($tmpfile) or warn("WARNING: Error removing tmpfile, $tmpfile: $!\n");
	
	# DONE
	my $tot = $numAmplified + $numUnamplified;
	my $pctAmplified = int($numAmplified/$tot*1000 + 0.5)/10;
	my $pctHighQuality = int($numHighQuality/$numAmplified*1000 + 0.5)/10;
	warn("INFO: $numAmplified sequences ($pctAmplified%) were amplified; of which, $numHighQuality ($pctHighQuality%) sequences are of sufficient quality to use as taxonomic references\n");
}

=item checkForDuplicateAmplicons

Check if any amplicon sequence occurs at more than one node.

=cut

# NYI
sub checkForDuplicateAmplicons
{
	my ($usearch, $root, $infile, $outfile) = @_;
	my $tmpfile = "$outfile.tmp";
	my @cmd = qq($usearch -search_exact $infile -db $infile -blast6out $tmpfile -strand plus);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");
	open(my $in, '<', $tmpfile) or die($!);
	open(my $out, '>', $outfile) or die($!);
	while (<$in>)
	{
		chomp;
		my ($acc1, $acc2) = split(/\t/);
		next if $acc1 eq $acc2;
		$acc1 =~ /^(.+):\d+/ and $acc1 = $1; # remove taxon id
		$acc2 =~ /^(.+):\d+/ and $acc2 = $2;
		my $node1 = $root->accToNode($acc1);
		my $node2 = $root->accToNode($acc2);
		unless ( defined($node1) )
		{
			warn("Node not defined for acc $acc1\n");
			next;
		}
		unless ( defined($node2) )
		{
			warn("Node not defined for acc $acc2\n");
			next;
		}
		my $id1 = $node1->id;
		my $id2 = $node2->id;
		if ( $id1 ne $id2 )
		{
			print $out "Acc $acc1 (node $id1) has same amplicon sequence as acc $acc2 (node $id2)\n";
		}
	}
	close($in);
	close($out);
	unlink($tmpfile);
}

=item generateUtaxFiles

Perform training and generate UDB file.

=cut

sub generateUtaxFiles
{
	my ($usearch, $infile, $confOutfile, $reportOutfile, $udbOutfile) = @_;
	$infile or die("FASTA file of high-quality reference amplicons required\n");

	# generate utaxconf training file
	my @cmd = qq($usearch -utax_train $infile -taxconfsout $confOutfile -report $reportOutfile -utax_splitlevels NVdpcofg -utax_trainlevels dpcofg);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");

	# generate udb file
	@cmd = qq($usearch -makeudb_utax $infile -taxconfsin $confOutfile -output $udbOutfile);
	system(@cmd) == 0 or die("Nonzero exit status for command: @cmd\n");
}

__END__
