#!/usr/bin/env perl

=pod

=head1 NAME

itaggerDiversity.pl

=head1 DESCRIPTION

Make Biom file and run QIIME core_diversity_analyses.  Multithreading not supported.

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration (*.C<ini>) infile.

=item --counts C<file>

OTU read counts infile.

=item --tax C<file>

OTU taxonomic classification infile.

=item --msa C<file>

Multiple sequence alignment infile.

=item --tree C<file>

Phylogenetic tree outfile.

=item --biom C<file>

OTU JSON biom outfile.

=item --map C<file>

QIIME mapping outfile.

=item --dir C<folder>

QIIME core_diversity_analyses output folder.

=item --log C<file>

JSON log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use File::Copy;
use File::Spec qw(rel2abs);
use File::Path qw(make_path remove_tree);
use JSON;
use File::Slurp;
use Config::Simple;
use iTagger::FastaDb;
use Env qw(TMPDIR USEARCH64 ITAGGER_CONFIG_DIR);
use File::Which;
use File::Basename;
use iTagger::Stats;
use Config::Simple;

our $VERSION = '3.0';
our $start = time;
our $json = new JSON;
our %log = ();

# OPTIONS
my $configFile;
my $countsInfile;
my $taxInfile;
my $msaInfile;
my $treeOutfile;
my $biomOutfile;
my $mappingOutfile;
my $outdir;
my $logfile;
my $overwrite;
my $help;
my $man;
GetOptions(
	'config=s' => \$configFile,
	'counts=s' => \$countsInfile,
	'tax=s' => \$taxInfile,
	'msa=s' => \$msaInfile,
	'tree=s' => \$treeOutfile,
	'biom=s' => \$biomOutfile,
	'map=s' => \$mappingOutfile,
	'dir=s' => \$outdir,
	'log=s' => \$logfile,
	'overwrite' => \$overwrite,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$configFile or die("--config file required\n");
$countsInfile or die("--counts file required\n");
$taxInfile or die("--tax infile required\n");
$msaInfile or die("--msa infile required\n");
$treeOutfile or die("--tree outfile required\n");
$biomOutfile or die("--biom outfile required\n");
$mappingOutfile or die("--map outfile required\n");
$outdir or die("--dir required\n");
$logfile or die("--log file required\n");

# LOG
$logfile =~ s/\.done$//i;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
-e $passLogFile and die("Operation tagged as done; not overwriting\n");
-d $outdir and remove_tree($outdir);
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");

# WRITE BIOM AND QIIME MAPPING FILES
makeFiles($countsInfile, $taxInfile, $biomOutfile, $mappingOutfile);

# PHYLOGENETIC TREE
$cmd="make_phylogeny.py -i $msaInfile -o $treeOutfile"; # TODO: -t $treeMethod
system($cmd) == 0 or fail("ERROR making tree via $cmd: $!\n");

# Run QIIME's core diversity analyses.
my $samplingDepth = $config->param('DIVERSITY.SAMPLING_DEPTH');
$cmd="core_diversity_analyses.py -i $biomOutfile -o $outdir -m $mappingOutfile -t $treeOutfile -e $samplingDepth";
system($cmd) == 0 or die("ERROR running QIIME via $cmd: $!\n");
done();
exit;

sub done
{
	$log{MINUTES} = int((time-$start)/60+0.5);
	write_file($passLogFile, $json->pretty->encode(\%log));
	exit;
}

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	$log{MINUTES} = int((time-$start)/60+0.5);
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler { fail("INTERRUPTED: $!") }

sub makeFiles
{
	my ($countsInfile, $taxInfile, $biomOutfile, $mappingOutfile) = @_;

	# READ COUNTS FILE
	my %otus;
	open($in, '<', $countsInfile) or die($!);
	my $hdr = <$in>;
	chomp $hdr;
	my ($x, @samples) = split(/\t/, $hdr);
	@samples = map { correctSampleId($_) } @samples;
	$hdr = join("\t", $x, @samples);
	while (<$in>)
	{
		chomp;
		my ($id, @counts) = split(/\t/);
		$otus{$id} = \@counts;
	}
	close($in);

	# ADD TAX COLUMN TO OTU TABLE
	my $n = 0;
	my $tmpfile = "$biomOutfile.txt";
	open(my $in, '<',  $taxInfile) or die($!);
	open(my $out, '>', $tmpfile) or die($!);
	print $out $hdr, "\ttaxonomy\n";
	while (<$in>)
	{
		my ($id, $full, $brief, $strand) = split(/\t/);
		exists($otus{$id}) or next;
		$brief eq '*' and next;
		print $out join("\t", $id, @{$otus{$id}}, $brief), "\n";
		++$n;
	}
	close($in);
	close($out);
	$n or die("No OTUs have maps reads and taxonomic classification\n");

	# CONVERT TO BIOM JSON
	my $cmd = "biom convert -i $tmpfile -o $biomOutfile --to-json --table-type='OTU table' --process-obs-metadata taxonomy";
	system($cmd) == 0 or die("ERROR running $cmd");
	unlink($tmpfile);

	# GENERATE MAPPING FILE IN QIIME'S FORMAT
	open($out, '>', $mappingOutfile) or die("Unable to write mapping file, $mappingOutfile: $!\n");
	print $out "#SampleID\tLinkerPrimerSequence\n";
	foreach my $sample ( @samples ) { print $out "$sample\t\n" }
	close($out);

	# VALIDATE MAPPING FILE
#	my $output = `validate_mapping_file.py -m $mappingOutfile`;
#	$? == 0 or die("Error running validate_mapping_file.py: $!\n");
#	$output =~ m/Errors and\/or warnings detected in mapping file/ and die("Mapping file invalid\n");
}

sub correctSampleId
{
	my $id = shift;
	$id =~ s/[^A-Za-z0-9]/./g;
	$id !~ /^[A-Za-z]/ and $id = "S$id";
	return $id;
}

__END__

# Alpha and beta diversity; add mean shannon10 to log
sub diversityAnalysis
{
	my ($config, $otuFile, $alphaFile, $betaDir, $log, $threads) = @_;
	my $alphaMetrics = $config->param('DIVERSITY.ALPHA'); # e.g. 'shannon_10'
	my $betaMetrics = $config->param('DIVERSITY.BETA'); # e.g. 'bray_curtis'
	
	# CHECK INFILE
	-f $otuFile or return;
	open(my $in, '<', $otuFile) or die($!);
	my $hdr = <$in>;
	my @hdr = split(/\t/, $hdr);
	my $numSamples = @hdr - 1;
	my $numOtus = 0;
	while (<$in>) { ++$numOtus }
	close($in);
	$numOtus or return;

	# ALPHA
	if ( $alphaMetrics )
	{
		$alphaMetrics = lc($alphaMetrics);
		my $cmd = "$usearch -alpha_div $otuFile -output $alphaFile";
		if ( $alphaMetrics ne 'all' )
		{
			$cmd .= " -metrics $alphaMetrics";
			$alphaMetrics =~ /shannon_10/ or $cmd .= ',shannon_10'; # always calc shannon_10
		}
		my $output = `$cmd 2>&1`;
		$? == 0 or die("Error running $cmd\n$output\n");
		my $shannon10index;
		my @shannon10;
		open($in, '<', $alphaFile) or die($!);
		while (<$in>)
		{
			if (/^Sample/)
			{
				chomp;
				my @row = split(/\t/);
				for (my $i=1; $i<=$#row; $i++)
				{
					if ($row[$i] eq 'shannon_10')
					{
						$shannon10index = $i;
						last;
					}
				}
				last;
			}
		}
		while (<$in>)
		{
			chomp;
			my @row = split(/\t/);
			push @shannon10, $row[14];
		}
		close($in);
		my ($alphaStdDev, $alphaMean) = stdDev(\@shannon10);
		$log->{ALPHA_MEAN} = $alphaMean;
		$log->{ALPHA_STDEV} = $alphaStdDev;
	}

	# BETA
	$numSamples > 1 or return; 
	if ( $betaMetrics )
	{
		-d $betaDir or mkdir($betaDir) or die("Unable to mkdir $betaDir: $!\n");
		$cmd = "$usearch -beta_div $otuFile -filename_prefix $betaDir -threads $threads";
		$betaMetrics and $cmd .= " -metrics $betaMetrics";
		$output = `$cmd 2>&1`;
		$? == 0 or die("Error running $cmd\n$output\n");
	}
}
