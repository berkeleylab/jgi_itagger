#!/usr/bin/env perl

=pod

=head1 NAME

itaggerMapping.pl

=head1 DESCRIPTION

OTU clustering (by usearch cluster_otus and unoise2) and classification (via usearch utax and sintax).    
This script performs all the actions requiring 64-bit usearch (licensed).  This script does not manage    
the license, that is the responsibility of the pipeline (itaggerJgi.pl).

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration file in C<ini> format.

=item --in C<file>

Reads FASTA (query) infile(s).  This option may be used multiple times.

=item --db C<file>

OTU FASTA (reference) infile.

=item --out C<file>

OTU counts table outfile.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item --threads C<int>

Number of threads to use.  Optional; default=2.

=item -- C<metadata>

Optionally add metadata to logfile by providing one or more C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov), Julien Tremblay

=head1 COPYRIGHT/LICENSE

Copyright 2013 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

This pipeline wraps USEARCH and VSEARCH; refer to their documentation for description, license, and copyright information.

=cut

use strict;
use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use Env qw(TMPDIR USEARCH32 ITAGGER_CONFIG_DIR);
use Config::Simple;
use JSON;
use File::Slurp;

our $VERSION = '3.0';
our $start = time;
our %log = ();
our $json = new JSON;
our $threads = 2;

# OPTIONS
my $configFile;
my @infiles;
my $otuInfile;
my $outfile;
my $logfile;
my $overwrite;
my $help;
my $man;
GetOptions(
    'config=s' => \$configFile,
	'in=s' => \@infiles,
	'db=s' => \$otuInfile,
	'out=s' => \$outfile,
	'log=s'=> \$logfile,
    'threads=i' => \$threads,
	'overwrite' => \$overwrite,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
@infiles or die("No reads files provided\n");
$configFile or die("--config file required\n");
$otuInfile or die("--otu file required\n");
$outfile or die("--out file required\n");
$logfile or die("--log file required\n");

# LOG
$logfile =~ s/\.done$//;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
if ( -e $passLogFile and ! $overwrite and -e $outfile )
{
	warn("Not overwriting previous results\n");
	exit;
} else
{
	unlink($passLogFile, $outfile);
}
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");

# COUNT OTUS
$log{NUM_OTUS} = `grep -c '^>' $otuInfile` + 0;

# MAP READS
if ( @infiles > 1 )
{
	mapMultiple($config, $otuInfile, \@infiles, $outfile, $threads, \%log);
} else
{
	my $infile = shift @infiles;
	eval { mapReads($config, $otuInfile, $infile, $outfile, $threads, \%log) };
	if ( $@ ) { fail("MAPPING FAILED: $@\n") }
}
done();

sub done
{
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($passLogFile, $json->pretty->encode(\%log));
	exit;
}

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler { fail("INTERRUPTED: $!") }

sub mapReads
{
	my ($config, $subjFile, $queryFile, $tableOutfile, $threads, $log) = @_;
	$subjFile or die("OTU FASTA file required");
	$queryFile or die("Input reads FASTA required");
	$tableOutfile or die("OTU table tableOutfile required");
	$threads or $threads = 1;
	my $algo = $config->param('MAPPING.METHOD');
	$algo or $algo = 'VSEARCH';
	$log->{METHOD} = $algo;
	if ( $algo eq 'USEARCH' )
	{
		mapUsearch($config, $subjFile, $queryFile, $tableOutfile, $threads, $log);	
	} elsif ( $algo eq 'VSEARCH' )
	{
		mapVsearch($config, $subjFile, $queryFile, $tableOutfile, $threads, $log);	
	} else
	{
		die("Invalid mapper: $algo\n");
	}
}

sub mapUsearch
{
	my ($config, $subjFile, $queryFile, $tableOutfile, $threads, $log) = @_;

	my $minIdentity = $config->param('MAPPING.MIN_IDENTITY');
	$minIdentity <= 1 or die("Invalid identity threshold: $minIdentity");
	my $maxAccepts = $config->param('MAPPING.MAX_ACCEPTS');
	my $maxRejects = $config->param('MAPPING.MAX_REJECTS');

	my $cmd = $USEARCH32 ? $USEARCH32 : 'usearch';
	$cmd .= " -usearch_global $queryFile -db $subjFile -strand plus -id $minIdentity -otutabout $tableOutfile -threads $threads -maxaccepts $maxAccepts -maxrejects $maxRejects";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running command: $cmd\n$output\n");
	if ( $output =~ /(\d+) \/ (\d+) mapped to OTUs/)
	{
		my $numMatched = $log->{NUM_MATCHED} = $1 + 0;
		my $numIn = $log->{NUM_READS} = $2 + 0;
		$log->{PCT_MATCHED} = $numIn ? int($numMatched/$numIn*100 + 0.5) : undef;
	}
}

sub mapVsearch
{
	my ($config, $subjFile, $queryFile, $tableOutfile, $threads, $log) = @_;

	my $minIdentity = $config->param('MAPPING.MIN_IDENTITY');
	$minIdentity <= 1 or die("Invalid identity threshold: $minIdentity");
	my $maxAccepts = $config->param('MAPPING.MAX_ACCEPTS');
	my $maxRejects = $config->param('MAPPING.MAX_REJECTS');

	# MAPPING
	my $cmd = "vsearch -usearch_global $queryFile -db $subjFile -strand plus -id $minIdentity -otutabout $tableOutfile -threads $threads -maxaccepts $maxAccepts -maxrejects $maxRejects";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running command: $cmd\n$output\n");
	if ( $output =~ /Matching query sequences: (\d+) of (\d+)/)
	{
		my $numMatched = $log->{NUM_MATCHED} = $1 + 0;
		my $numIn = $log->{NUM_READS} = $2 + 0;
		$log->{PCT_MATCHED} = $numIn ? int($numMatched/$numIn*100 + 0.5) : undef;
	}
}

sub mapMultiple
{
	my ($config, $subjFile, $queryFiles, $outfile, $threads, $log) = @_;
	my @tmpfiles;
	my @logs;
	for ( my $i=0; $i < scalar(@$queryFiles); ++$i )
	{
		$tmpfiles[$i] = "$TMPDIR/$$.MAPPING.$i.txt";
		$logs[$i] = {};
		eval { mapReads($config, $subjFile, $queryFiles->[$i], $tmpfiles[$i], $threads, $logs[$i]) };
		if ( $@ ) { fail("MAPPING FAILED: $@\n") }
	}
	mergeOtuTables(\@tmpfiles, \@logs, $outfile, \%log);
	unlink(@tmpfiles);
}

# merge OTU tables which may not have identical sample columns
sub mergeOtuTables
{
	my ($infiles, $logs, $outfile, $log) = @_;
	$log->{NUM_READS} = 0;
	$log->{NUM_MATCHED} = 0;

	# GET ALL SAMPLE IDS AND MERGE LOGS.  DON'T READ COUNTS YET.
	my %samples; # sample ID => index on counts
	for (my $i=0; $i<=$#$infiles; ++$i)
	{
		$log->{NUM_READS} += $logs->[$i]->{NUM_READS};
		$log->{NUM_MATCHED} += $logs->[$i]->{NUM_MATCHED};
		open(my $in, '<', $infiles->[$i]) or die($!);
		my $hdr = <$in>;
		close($in);
		chomp $hdr;
		my ($otu, @samples) = split(/\t/, $hdr);
		$samples{$_} = undef foreach @samples;
	}

	# DEFINE SAMPLE COLUMN ORDER AND SAVE OFFSETS
	my @samples = sort keys %samples;
	for (my $i=0; $i<=$#samples; ++$i)
	{
		$samples{$samples[$i]}=$i;
	}

	# LOAD READ COUNTS
	my %otus; # OTU id => [ counts ]
	foreach my $infile ( @$infiles )
	{
		open(my $in, '<', $infile) or die($!);
		my $hdr = <$in>;
		chomp $hdr;
		my ($o, @s) = split(/\t/, $hdr);
		while (<$in>)
		{
			chomp;
			my ($otu, @counts) = split(/\t/);
			if ( ! exists($otus{$otu}) )
			{
				my @c = split(//, '0' x scalar(@samples));
				$otus{$otu} = \@c;
			}
			for ( my $i=0; $i<=$#counts; ++$i )
			{
				my $j = $samples{$s[$i]};
				$otus{$otu}->[$j] += $counts[$i];
			}
		}
		close($in);
	}

	# OUTPUT
	open(my $out, '>', $outfile) or die($!);
	print $out join("\t", '#OTU ID', @samples), "\n";
	foreach my $otu ( sort keys %otus )
	{
		print $out join("\t", $otu, @{$otus{$otu}}), "\n";
	}
	close($out);
	$log->{PCT_MATCHED} = $log->{NUM_READS} ? int($log->{NUM_MATCHED}/$log->{NUM_READS}*100 + 0.5) : undef;
}
