#!/usr/bin/env perl

=pod

=head1 NAME

itaggerReadQc.pl

=head1 DESCRIPTION

Preprocess a single sample's reads.  Merge read-pairs into single consensus, verify presence, orientation 
and expected distance between PCR primers, trim primers, discard low-quality sequences, dereplicate       
identical sequences.

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration file in C<.ini> format.

=item --sample C<string>

Sample ID, which is added to read headers.

=item --in C<file>

Input reads FASTQ file, either collated or split read-pairs.  If separate R1/R2 files, only provide R1 file.

=item --high C<file>

High quality reads FASTA outfile.

=item --low C<file>

Low quality reads FASTA outfile.

=item --epcr

Only perform the EPCR step if specified in config file and this flag is provided.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=item --tmpdir C<string>

Temporary dir.  Optional; default=C<$TMPDIR>

=item --threads C<int>

Number of threads to use.  Optional; default=1.

=cut

=back

=head1 NOTES

This script uses USEARCH; the 32 bit executable is adequate.

=head1 AUTHORS

Edward Kirton (ESKirton@lbl.gov), Julien Tremblay

=head1 CREDITS

This scripts wraps Robert Edgar's USEARCH.

=head1 COPYRIGHT

Copyright (c) 2013 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.      
Refer to the documentation of usearch (http://drive5.com) for it's copyright/license information.

=cut

use strict;
use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use Env qw(TMPDIR ITAGGER_CONFIG_DIR USEARCH32);
use Config::Simple;
use iTagger::FastaDb;
use iTagger::FastqDb;
use File::Basename;
use File::Path qw(remove_tree make_path);
use File::Copy;
use JSON;
use File::Slurp;
use File::Which;

# VALIDATE ENV
$TMPDIR or die("\$TMPDIR not defined\n");
-d $TMPDIR or die("\$TMPDIR does not exist: $TMPDIR\n");

# INIT VARS
our $VERSION = '3.0';
our $json = new JSON;
our $usearch;
if ( $USEARCH32 ) { $usearch = $USEARCH32 }
elsif ( $usearch = which('usearch32') ) { 1 }
elsif ( $usearch = which('usearch') ) { 1 }
else { die("usearch executable not found in \$USEARCH32 or \$PATH\n") }
chomp $usearch;
-f $usearch or die("usearch executable not found: $usearch\n");
$TMPDIR or die("\$TMPDIR not defined\n");
-d $TMPDIR or die("\$TMPDIR does not exist: $TMPDIR\n");

# PARAMETERS
my $verbose;
my $threads = 1;
my $tmpdirRoot = $TMPDIR;
my $configFile;
my $sampleId;
my $infile;
my $highQualOutfile;
my $lowQualOutfile;
my $logfile;
my $libName;
my $overwrite;
my $keep;
my $epcr;
my $help;
my $man;
GetOptions(
	'verbose' => \$verbose,
	'overwrite' => \$overwrite,
    'keep' => \$keep,
    'tmpdir=s' => \$tmpdirRoot,
    'config=s' => \$configFile,
    'sample=s' => \$sampleId,
	'lib=s' => \$libName,
    'in=s' => \$infile,
    'high=s' => \$highQualOutfile,
	'low=s' => \$lowQualOutfile,
	'log=s' => \$logfile,
	'threads=i' => \$threads,
	'epcr' => \$epcr,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$configFile or die("--config file required\n");
$infile or die("--in file required\n");
$highQualOutfile or die("--high qual FASTA outfile required\n");
$lowQualOutfile or die("--low qual FASTA outfile required\n");
$logfile or die("--log file required\n");
$sampleId or die("--sample ID required\n");
$sampleId =~ /^\w+$/ or die("--sample $sampleId contains invalid characters\n");

# TMPDIR
my $tmpdir= "$tmpdirRoot/$$.".int(rand(999999));
make_path($tmpdir) or die("Unable to mkdir $tmpdir: $!");

# CONFIG FILE
my $config = Config::Simple->new($configFile) or die("Unable to load config, $configFile: $!");

# LOG
our %log =
(
	SAMPLE_ID => $sampleId,
	INFILE => $infile,
	PASS_FLAG => 0
);
$logfile =~ s/\.done$//i;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
if ( -e $passLogFile and ! $overwrite and -e $highQualOutfile )
{
	warn("Not overwriting previous results\n");
	exit;
} else
{
	unlink($passLogFile, $highQualOutfile);
}
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# DEFINE TEMP FILES
my $basename = basename($logfile);
-d $tmpdir or make_path($tmpdir) or die("Unable to mkdir $tmpdir: $!");
my $reads1File = "$tmpdir/${basename}_R1_reads.fastq";
my $reads2File = "$tmpdir/${basename}_R2_reads.fastq";
my $trimmed1File = "$tmpdir/${basename}_R1_trimmed.fastq";
my $trimmed2File = "$tmpdir/${basename}_R2_trimmed.fastq";
my $mergedFile = "$tmpdir/$basename.merged.fastq";
my $hardTrimmedFile = "$tmpdir/$basename.hardTrimmed.fastq";
my $epcrFile = "$tmpdir/$basename.epcr.fastq";

# SPLIT AND/OR UNCOMPRESS INPUT FILES AS NECESSARY
my $n0;
my $n1;
$verbose and print "Preparing infile\n";
eval
{
	$n1 = prepareInfile(\%log, $infile, $reads1File, $reads2File, $sampleId);
};
$@ and fail($failLogFile, "PREPARE INFILE FAILED: $@");
my $numInput = $n1 or done($passLogFile, 0, 0);

# TRIM PRIMERS
$n0=$n1;
$verbose and print "Trimming $n0 PCR primers\n";
my $spacer1len = my $spacer2len = undef; # learn if not specified
eval
{
	$n1 = trimPrimers
	(
		\%log,
		$reads1File,
		$reads2File, 
		$config->param('READ_QC.FWD_PRIMER_FASTA'),
		$config->param('READ_QC.REV_PRIMER_FASTA'),
		$config->param('READ_QC.FWD_PRIMER_MAX_DIFFS'),
		$config->param('READ_QC.REV_PRIMER_MAX_DIFFS'),
		$config->param('READ_QC.READ_MIN_LEN'),
		$spacer1len,
		$spacer2len,
		$trimmed1File,
		$trimmed2File,
		$threads
	);
};
$keep or unlink($reads1File, $reads2File);
$@ and fail($failLogFile, "TRIM PRIMERS FAILED: $@");
$log{PRIMER_PCT} = int($n1/$n0*100+0.5);
$n1 or return done($passLogFile, $numInput, 0);

# MERGE PAIR READS INTO CONSENSUS READ AND ADD SAMPLE NAME TO HEADER
$n0=$n1;
$verbose and print "Merging $n0 pairs\n";
eval
{
	$n1 = mergePairs
	(
		\%log,
		$trimmed1File,
		$trimmed2File,
		$mergedFile,
		$sampleId,
		$config->param('READ_QC.MERGE_MAX_DIFF_PCT'),
		$config->param('READ_QC.AMPLICON_MIN_LEN'),
		$config->param('READ_QC.AMPLICON_MAX_LEN'),
		$threads
	);
};
$keep or unlink($trimmed1File, $trimmed2File);
$@ and fail($failLogFile, "MERGE PAIRS FAILED: $@");
$log{MERGED_PCT} = int($n1/$n0*100+0.5);
$n1 or return done($passLogFile, $numInput, 0);

# EPCR
$n0=$n1;
if ( $epcr )
{
	$verbose and print "Electronic PCR on $n0 sequences\n";
	my $primerFasta;
	if ( $primerFasta = $config->param('EPCR.REV_PRIMER_FASTA') )
	{
		$n1 = trimRevPrimerOnly
		(
			\%log,
			$mergedFile, 
			$primerFasta,
			$epcrFile,
			$config->param('EPCR.MAX_DIFFS')
		);
	}
	elsif ( $primerFasta = $config->param('EPCR.PRIMER_FASTA') )
	{
		$n1 = epcr
		(
			\%log,
			$mergedFile, 
			$primerFasta,
			$epcrFile,
			$config->param('EPCR.MAX_DIFFS')
		);
	}
	elsif ( $primerFasta = $config->param('EPCR.FWD_PRIMER_FASTA') )
	{
		die("NYI"); # TODO
	}
	else
	{
		die("EPCR primer not specified in config file\n");
	}
	$log{EPCR_PCT} = int($n1/$n0*100+0.5);
}
else
{
	$n1=$n0;
	move($mergedFile, $epcrFile);
}

# FILTER LOW-QUALITY READS
$n0=$n1;
$verbose and print "Filtering $n0 reads\n";
eval
{
	$n1 = qualFilter
	(
		\%log,
		$epcrFile,
		$highQualOutfile,
		$lowQualOutfile,
		$config->param('READ_QC.MAX_EE'),
		$threads
	);
};
$@ and fail($failLogFile, "QUALITY FILTER FAILED: $@");
$log{QUAL_FILTERED_PCT} = int($n1/$n0*100+0.5);
$n1 or return done($passLogFile, $numInput, 0);

# PASS OR FAIL
my $numOutput = $n1;
my $pctOut = int($numOutput/$numInput*100 + 0.5);
my $minNum = $config->param('READ_QC.MIN_NUM_PASS');
$minNum or $minNum = 500;
my $minPct = $config->param('READ_QC.MIN_PCT_PASS');
$minPct or $minPct = 33;
if ( $numOutput >= $minNum and $pctOut >= $minPct ) { $log{PASS_FLAG} = 1 }
done($passLogFile, $numInput, $numOutput);
exit;

sub done
{
	my ($passLogFile, $numIn, $numOut) = @_;
	$numIn and $log{OUTPUT_PCT} = int($numOut/$numIn*100 + 0.5);
	write_file($passLogFile, $json->pretty->encode(\%log));
	$keep or remove_tree($tmpdir);
	exit;
}

sub fail
{
	my ($failLogFile, $err) = @_;
	$keep or remove_tree($tmpdir);
	$log{ERROR} = $err;
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}


sub signal_handler { die("INTERRUPTED: $!\n") }

# If necessary, uncompress and/or split collated reads into separate R1/R2 files.  If no processing was required, simply creates symlink.  Returns number of read-pairs input.
sub prepareInfile
{
	my ($log, $infile, $outfile1, $outfile2, $sampleId) = @_;
	$infile or die("Missing required infile\n");
	-e $infile or die("Infile not found: $infile\n");
	$outfile1 or die("Missing required outfile\n");
	$outfile1 =~ /R1/ or die("Outfile requires R1 in name\n");
	$outfile2 =~ /R2/ or die("Outfile requires R2 in name\n");

	# LOG
	unless ( -s $infile )
	{
		$log->{INPUT_NUM} = 0;
		return;
	}

	my $numPairs = 0;
	if ( $infile =~ /R1/ )
	{
		# ALREADY SPLIT INTO R1/R2
		my $infile2 = $infile;
		$infile2 =~ s/R1/R2/;
		my @cmd;
		my $numPairs2 = 0;
		if ( $infile =~ /[\.gz|\.bz2]$/ )
		{
			# UNCOMPRESS R1
			my $db = iTagger::FastqDb->new($infile);
			open(my $out, '>', $outfile1) or die("Unable to open outfile, $outfile1: $!\n");
			while ( my $read = $db->next_seq )
			{
				print $out $read->output;
				++$numPairs;
			}
			close($out);

			# UNCOMPRESS R2
			$db = iTagger::FastqDb->new($infile);
			open($out, '>', $outfile2) or die("Unable to open outfile, $outfile2: $!\n");
			while ( my $read = $db->next_seq )
			{
				print $out $read->output;
				++$numPairs2;
			}
			close($out);

		} else
		{
			# COUNT AND CREATE SYMLINKS
			symlink($infile, $outfile1);
			symlink($infile2, $outfile2);
			$numPairs = countFastq($infile);
			$numPairs2 = countFastq($infile2);
		}

		# VERIFY READ COUNTS
		if ( $numPairs != $numPairs2 )
		{
			die("R1/R2 files don't contain same number of reads ($numPairs, $numPairs2)\n");
		}
	} else
	{
		$numPairs = splitFastqR1R2($infile, $outfile1, $outfile2);
	}
	$log->{INPUT_NUM} = $numPairs;
	return $numPairs;
}

# Split collated FASTQ file into separate R1/R2 files. Returns number of read-pairs.
sub splitFastqR1R2
{
	my ($infile, $r1file, $r2file) = @_;
	$infile or die("Infile required");
	-f $infile or die("Infile not found: $infile\n");
	if ( $r1file )
	{
		$r1file =~ /_R1/ or die("Output file doesn't contain _R1 as required");
		if ( ! $r2file )
		{
			$r2file = $r1file;
			$r2file =~ s/_R1/_R2/;
		}
	} else
	{
		my ($basename, $folder, $suffix) = fileparse($infile, qw(.fastq .fq .fastq.gz .fq.gz .fastq.bz2 .fq.bz2));
		$r1file = "$folder/${basename}_R1.fastq";
		$r2file = "$folder/${basename}_R2.fastq";
	}

	# SPLIT
	my $numPairs = 0;
	my $db = iTagger::FastqDb->new($infile, { paired=>1 });
	open(my $out1, '>', $r1file) or die("Unable to open outfile, $r1file: $!\n");
	open(my $out2, '>', $r2file) or die("Unable to open outfile, $r2file: $!\n");
	while ( my $pr = $db->next_pair )
	{
		++$numPairs;
		print $out1 $pr->[0]->output;
		print $out2 $pr->[1]->output;
	}
	return $numPairs;
}

# Returns the number of sequences in a Fastq file (individual seqs, not pairs)
sub countFastq
{
	my $file = shift;
	-f $file or die("Can't count seqs in fastq file, $file: file not found\n");
	-s $file or return 0;
	my $result;
	if ( $file =~ /\.gz$/ )
	{
		my $line = `gunzip -c $file | head -1`;
		$line =~ /^@/ or die("Invalid fastq file: $file");
		$result = `gunzip -c $file | wc -l`;
	} elsif ( $file =~ /\.bz2$/ )
	{
		my $line = `bzcat $file | head -1`;
		$line =~ /^@/ or die("Invalid fastq file: $file");
		$result = `bzcat $file | wc -l`;
	} elsif ( $file =~ /\.zip$/ )
	{
		my $line = `unzip -p $file | head -1`;
		$line =~ /^@/ or die("Invalid fastq file: $file");
		$result = `unzip -p $file | wc -l`;
	} else
	{
		my $line = `head -1 $file`;
		$line =~ /^@/ or die("Invalid fastq file: $file");
		$result = `wc -l $file`;
	}
	chomp $result;
	my @result = split(/\s+/, $result);
	my $n = shift @result;
	$n % 4 and die("Invalid fastq file, possibly truncated: $file");
	return $n/4;
}

# Find and trim off forward and reverse PCR primers from unmerged read pairs.  Primers are matched separately for reporting purposes.  Uses usearch's search_oligodb.  Only pairs in which both primers were found are output.  Pairs are not merged.  Requires that all sequences in this file have a random-NT spacer of the same length.  Returns hashref of summary stats: numFwdFound, numRevFound, numPairsIn, numPairsOut
sub trimPrimers
{
	my ($log, $infile1, $infile2, $primerFile1, $primerFile2, $maxdiffs1, $maxdiffs2, $readMinLen, $spacer1, $spacer2, $outfile1, $outfile2, $threads) = @_;
	$threads or $threads = 1;

	# DETERMINE PRIMER LENGTHS
	my $db = iTagger::FastaDb->new($primerFile1) or die("Unable to open fwd primers file: $primerFile1\n");
	my $seq = $db->next_seq;
	my $fwdLen = $seq->len;
	$fwdLen or die("Invalid forward primer\n");
	#print "\tFwd primer is $fwdLen bp\n";
	$db = iTagger::FastaDb->new($primerFile2) or die("Unable to open rev primers file: $primerFile2\n");
	$seq = $db->next_seq;
	my $revLen = $seq->len;
	$revLen or die("Invalid reverse primer\n");
	#print "\tRev primer is $revLen bp\n";

	# DEFINE TMPFILES
	my $tmpfile1 = "$outfile1.tmp";
	my $tmpfile2 = "$outfile2.tmp";

	# SEARCH
	my $cmd = "$usearch -search_oligodb $infile1 -db $primerFile1 -strand plus -maxdiffs $maxdiffs1 -userout $tmpfile1 -threads $threads -userfields query+qlo+qhi+diffs 2>&1";
	my $output = `$cmd`;
	$? == 0 or die("ERROR during primer search: $cmd\n");
	$cmd = "$usearch -search_oligodb $infile2 -db $primerFile2 -strand plus -maxdiffs $maxdiffs2 -userout $tmpfile2 -threads $threads -userfields query+qlo+qhi+diffs 2>&1";
	$output = `$cmd`;
	$? == 0 or die("ERROR during primer search: $cmd\n");

	# DETERMINE SPACER LENGTHS IF NECESSARY
	defined($spacer1) or $spacer1 = _determineSpacerLength($tmpfile1);
	defined($spacer2) or $spacer2 = _determineSpacerLength($tmpfile2);
	$log->{PRIMER_FWD_SPACER_LEN} = $spacer1;
	$log->{PRIMER_REV_SPACER_LEN} = $spacer2;

	# FIND VALID FORWARD PRIMER MATCHES
	my $validStart1 = $spacer1 + 1;
	my $validStart2 = $spacer2 + 1;
	my $validEnd1 = $validStart1 + $fwdLen - 1;
	my $validEnd2 = $validStart2 + $revLen - 1;
	my $fwdFound = _getValidPrimerMatches($tmpfile1, $validStart1, $validEnd1, $maxdiffs1);
	my $revFound = _getValidPrimerMatches($tmpfile2, $validStart2, $validEnd2, $maxdiffs2);
	$log->{PRIMER_FWD_SPACER_NUM} = scalar(keys %$fwdFound);
	$log->{PRIMER_REV_SPACER_NUM} = scalar(keys %$revFound);
	unlink($tmpfile1, $tmpfile2);

	# OUTPUT SUBSEQUENCES
	open(my $out1, '>', $outfile1) or die($!);
	open(my $out2, '>', $outfile2) or die($!);
	my $in1 = iTagger::FastqDb->new($infile1);
	my $in2 = iTagger::FastqDb->new($infile2);
	my $numPairsIn = my $numPairsOut = 0;
	while ( my $fwd = $in1->next_seq )
	{
		my $rev = $in2->next_seq;
		++$numPairsIn;
		exists($fwdFound->{$fwd->id}) or next;
		exists($revFound->{$rev->id}) or next;
		$fwd->subseq($validEnd1) or next;
		$rev->subseq($validEnd2) or next;
		$fwd->len >= $readMinLen or next;
		$rev->len >= $readMinLen or next;
		print $out1 $fwd->output;
		print $out2 $rev->output;
		++$numPairsOut;
	}
	close($out1);
	close($out2);
	$log->{PRIMER_NUM} = $numPairsOut;
	$log->{PRIMER_PCT} = int($numPairsOut/$numPairsIn*100+0.5);
	return $numPairsOut;
}

# Parse search_oligodb output to determine spacer length.
sub _determineSpacerLength
{
	#print "Determining spacer length\n";
	my $file = shift;
	my ($id, $start, $end, $diffs);
	my %starts;
	my $spacer;
	open(my $in, '<', $file) or die($!);
	while (<$in>)
	{
		chomp;
		($id, $start, $end, $diffs) = split(/\t/);
		$diffs and next;
		$starts{$start} += 1;
	}
	close($in);
	my $max=0;
	foreach $start ( keys %starts )
	{
		if ( $starts{$start} > $max )
		{
			$max = $starts{$start};
			$spacer = $start - 1;
		}
	}
	#print "\tSpacer is $spacer bp\n";
	return $spacer;
}

# Parse search_oligodb output to get valid primer matches.
sub _getValidPrimerMatches
{
	my ($file, $validStart, $validEnd, $maxdiffs) = @_;
	my %results; # id => undef
	my ($id, $start, $end, $diffs);
	open(my $in, '<', $file) or die($!);
	while (<$in>)
	{
		chomp;
		($id, $start, $end, $diffs) = split(/\t/);
		$start == $validStart or next;
		$end == $validEnd or next;
		$diffs > $maxdiffs and next;
		$results{$id} = undef;
	}
	close($in);
	return \%results;
}

# Merge overlapping read-pairs into consensus reads using usearch's fastq_mergepairs.  Returns hashref of assorted metrics: numIn, numOut, numZeroDiff, numFwdShort, numRevShort, numNoAlign, numAlignShort, numStaggered, meanAlignLen, meanMergedLen, meanFwdEE, meanRevEE, meanMergedEE
sub mergePairs
{
	my ($log, $infile1, $infile2, $outfile, $sample, $maxdiffpct, $minmergelen, $maxmergelen, $threads) = @_;
	$threads or $threads = 1;
	my $cmd = "$usearch -fastq_mergepairs $infile1 -reverse $infile2 -fastqout $outfile -fastq_maxdiffpct $maxdiffpct -threads $threads -fastq_minmergelen $minmergelen -fastq_maxmergelen $maxmergelen";
	$sample and $cmd .= " -sample $sample";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running $cmd\n$output\n");
	my @output = split(/\n/, $output);
	my $numOutput;
	foreach (@output)
	{
		if (/^\s+(\d+)\s+Merged too short/) { $log->{NUM_MERGED_TOO_SHORT} = $1 }
		elsif (/^\s+(\d+)\s+Merged too long/) { $log->{NUM_MERGED_TOO_LONG} = $1 }
		elsif (/^\s+(\d+)\s+Mean merged length/) { $log->{MERGED_MEAN_LEN} = $1  }
		elsif (/^\s+(\d+\.\d+)\s+Mean alignment length/) { $log->{MERGED_OVERLAP_MEAN} = int($1+0.5) }
		elsif (/^\s+(\d+)\s+No alignment found/) { $log->{NUM_UNMERGED_NO_ALIGN} = $1 }
		elsif (/^\s+(\d+)\s+Too many diffs/) { $log->{NUM_UNMERGED_OVERLAP_DIFFS} = $1 }
		elsif (/^\s+(\d+)\s+Alignment too short/) { $log->{NUM_UNMERGED_OVERLAP_SHORT} = $1 }
		elsif (/^\s+(\d+)\s+Merged \([\d\.kM]+, \d+\.\d+%\)/) { $log->{NUM_MERGED} = $numOutput = $1 }
	}
	return $numOutput;
}

# Trim predefined number of bases from 5' and/or 3' ends of sequences.  Sequences that are too short are discarded.  Returns number of sequences output.
sub hardTrim
{
	my ($log, $infile, $outfile, $stripleft, $stripright, $min) = @_;
	# NOT USING USEARCH SINCE IT DOESN'T HAVE MINIMUM LENGTH FILTER 
	#my @cmd = qq($usearch -fastx_truncate $infile -stripleft $stripleft -stripright $stripright -fastqout $outfile);
	#system(@cmd) == 0 or die("Error running command: @cmd\n");

	my $numIn = 0;
	my $numFiltered = 0;
	my $numOut = 0;
	my $db = iTagger::FastqDb->new($infile);
	open(my $out, '>', $outfile) or die($!);
	while ( my $rec = $db->next_seq )
	{
		++$numIn;
		$rec->trim5_bp($stripleft);
		$rec->trim3_bp($stripright);
		if ( $rec->min_len($min) )
		{
			++$numOut;
			print $out $rec->output;
		} else
		{
			++$numFiltered;
		}
	}
	close($out);
	$log->{HARD_TRIMMED_NUM} = $numOut;
	$log->{HARD_TRIMMED_TOO_SHORT} = $numFiltered;
	return $numOut;
}

# Filters reads with too many expected errors.  Inputs FASTQ and outputs FASTA.  Returns number of reads input, discarded, output.  If outfiles have C<.gz> suffix then they will be compressed.
sub qualFilter
{
	my ($log, $infile, $hiQualFile, $lowQualFile, $maxee, $threads) = @_;
	$infile or die("Infile required");
	$hiQualFile or die("High quality outfile required");
	$lowQualFile or $lowQualFile = '/dev/null';
	$maxee or die("Maximum expected errors parameter required\n");
	$threads or $threads = 1;

	my $HQZ = $hiQualFile =~ s/\.gz$//i ? 1:0;
	my $LQZ = $lowQualFile =~ s/\.gz$//i ? 1:0;

	my $cmd = "$usearch -fastq_filter $infile -fastq_maxee $maxee -fastaout $hiQualFile -fastaout_discarded $lowQualFile -threads $threads";
	my $numInput;
	my $numOutput;
	open(my $in, "$cmd 2>&1|") or die($!);
	while (<$in>)
	{
		chomp;
		if (/^\s+(\d+)\s+Filtered reads/) { $log->{QUAL_FILTERED_NUM} = $numOutput = $1 }
		elsif (/^\s+(\d+)\s+Reads/) { $numInput = $1 }
	}
	close($in);

	if ( -s $hiQualFile )
	{
		$HQZ and system("gzip -9f $hiQualFile");
	} else
	{
		unlink($hiQualFile);
	}
	if ( -s $lowQualFile )
	{
		$LQZ and system("gzip -9f $lowQualFile");
	} else
	{
		unlink($lowQualFile);
	}
	return $numOutput;
}

# Amplification by EPCR, I.E. two primers.
sub epcr
{
	my ($log, $infile, $primersFile, $outfile, $maxdiffs, $minamp, $maxamp) = @_;

	# VALIDATE PRIMERS FILE
	my $db = iTagger::FastaDb->new($primersFile) or die("Unable to open primers file: $primersFile\n");
	my $fwd = $db->next_seq or die("Unable to read primers file\n");
	$fwd->id =~ /_FWD/ or die("Require first sequence to be forward primer and be named with _FWD suffix\n");
	my $rev = $db->next_seq or die("Require two primer sequences\n");
	$rev->id =~ /_REV/ or die("Require second sequence to be reverse primer and be named with _REV suffix\n");
	my $foo = $db->next_seq and die("Require exactly two primer sequences\n");
	my $fwdLen = $fwd->len;
	$fwdLen or die("Invalid forward primer length\n");
	my $revLen = $rev->len;
	$revLen or die("Invalid reverse primer length\n");
	my $primerLen = $fwdLen + $revLen;
	$minamp > $primerLen or $minamp = $primerLen + 13;
	$maxamp > $minamp or $maxamp = $minamp + 1;

	# SEARCH
	my $cmd = "$usearch -search_pcr $infile -db $primersFile -strand both -maxdiffs $maxdiffs -pcrout $outfile -threads 1";
	$minamp and $cmd .= " -minamp $minamp";
	$maxamp and $cmd .= " -maxamp $maxamp";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running $cmd\n$output\n");

	# PARSE OUTPUT
	my %amp;
	my $prev = '';
	my ($id, $start, $end, $seqlen, $primer1, $strand1, $diff1, $primer2, $strand2, $diff2, $offset, $len, $diffs);
	open(my $in, '<', $outfile) or die($!);
	while (<$in>)
	{
		chomp;
		($id, $start, $end, $seqlen, $primer1, $strand1, $diff1, $primer2, $strand2, $diff2) = split(/\t/);
		$strand1 eq '+' or next;
		$strand2 eq '-' or next;
		$primer1 =~ /_FWD$/ or next;
		$primer2 =~ /_REV$/ or next;
		$diff1 =~ s/\.//g;
		$diff2 =~ s/\.//g;
		$diffs = length($diff1) + length($diff2);
		$len = $end - $start + 1;
		if ( $id eq $prev )
		{
			$amp{$id}->[2] < $diffs and next; # skip if more mismatches
			$amp{$id}->[2] == $diffs and $amp{$id}->[1] > $len and next; # skip if same mismatches but shorter amplicon
		}
		$amp{$id} = [ $start - 1, $len, $diffs ]; # convert $start to 0-base offset
	}
	close($in);

	# OUTPUT SUBSEQUENCES (OVERWRITE OUTFILE WITH FASTQ)
	print "Writing subsequences to $outfile\n";
	open(my $out, '>', $outfile) or die($!);
	my $numOut = 0;
	$db = iTagger::FastqDb->new($infile);
	while ( my $rec = $db->next_seq )
	{
		exists($amp{$rec->base}) or next;
		($offset, $len) = @{$amp{$rec->base}};
		$rec->subseq($offset, $len) or next;
		++$numOut;
		print $out $rec->output;
	}
	close($out);
	return $numOut;
}

# If using amplicon sequences as input (i.e. primers already trimmed) and the target uses the same forward primer, then only the rev primer needs to be recognized.  This is used for amplifying V4 out of V4-V5 libraries.
sub trimRevPrimerOnly
{
	my ($log, $infile, $primersFile, $outfile, $maxdiffs, $minlen, $maxlen) = @_;
	$maxdiffs or $maxdiffs=4;

	my $tmpfile = "$outfile.out";
	my $cmd = "$usearch -search_oligodb $infile -db $primersFile -strand both -maxaccepts 1 -maxdiffs $maxdiffs -userout $tmpfile -userfields query+qstrand+qlo+qhi";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running $cmd\n$output\n");

	my %hits;
	open(my $in, '<', $tmpfile) or die($!);
	while (<$in>)
	{
		chomp;
		my ($id, $strand, $start, $end) = split(/\t/);
		$strand eq '-' or next;
		$hits{$id} = $start;
	}
	close($in);
	#unlink($tmpfile);

	my $numOut = 0;
	my $db = new iTagger::FastqDb($infile);
	open(my $out, '>', $outfile) or die($!);
	while ( my $rec = $db->next_seq )
	{
		exists($hits{$rec->base}) or next;
		my $start = $hits{$rec->base};
		$rec->subseq(0, $start-1);
		$minlen and $rec->len < $minlen and next;
		$maxlen and $rec->len > $maxlen and next;
		print $out $rec->output;
		++$numOut;
	}
	$log->{EPCR_NUM} = $numOut;
	return $numOut;
}

__END__
