#!/usr/bin/env perl

=pod

=head1 NAME

itaggerClustering.pl

=head1 DESCRIPTION

Cluster high-quality, dereplicated sequences (with sample tag in header) into OTU sequences with one of three algorithm choices.  Each are single threaded.  Usearch cluster_otus is the standard algorithm, vsearch is an open-source alternative which lacks chimera checking in the clustering loop, and unoise2 is Robert Edgar's latest denoising algorithm.  Run this script multiple times to generate multiple otu files; they are named automatically with the clustering algorithm as the basename.

=head1 OPTIONS

=over 5

=item --in C<file>

High quality reads FASTA file with sample defined in header.

=item --config C<file>

Configuration file in C<ini> format.

=item --out C<file>

OTU FASTA file.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item --threads C<int>

Number of threads, if supported by algorithm.  Optional; default=1.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

Refer to http://drive5.com for documentation, license, and copyright information for usearch.
Refer to https://github.com/torognes/vsearch for documentation, license, and copyright information for vsearch.

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use Config::Simple;
use JSON;
use File::Slurp;
use Config::Simple;
use File::Which;
use File::Basename;
use Env qw(TMPDIR USEARCH64 ITAGGER_CONFIG_DIR);

our $VERSION = '3.0';
our $start = time;
our $json = new JSON;
our %log;
our $vers = 1;
our $threads = 1;
our $usearch;
if ( $USEARCH64 ) { $usearch = $USEARCH64 }
elsif ( $usearch = which('usearch64') ) { 1 }
elsif ( $usearch = which('usearch') ) { 1 }
elsif ( $usearch = which('usearch32') ) { 1 }
else { $usearch = 'usearch' }
chomp $usearch;

my $configFile;
my $infile;
my $outfile;
my $logfile;
my $overwrite;
my $help;
my $man;

GetOptions(
	'in=s' => \$infile,
	'out=s' => \$outfile,
	'log=s' => \$logfile,
    'config=s' => \$configFile,
	'threads=i' => \$threads,
	'overwrite' => \$overwrite,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$infile or die("--in file required\n");
$outfile or die("--out file required\n");
$logfile or die("--log file required\n");

my %algorithms =
(
	'UCLUST' => \&usearchClusterOtus,
	'VCLUST' => \&vsearchClusterUchime,
	'UNOISE' => \&usearchUnoise2
);

# CONFIG
$configFile or die("--config file required\n");
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");
my $algo = $log{METHOD} = $config->param('CLUSTERING.METHOD') or die("Config missing METHOD parameter\n");
$log{VERSION} = $config->param('CLUSTERING.VERSION');
exists($algorithms{$algo}) or die("Unrecognized clustering algorithm: $algo\n");

# LOG
$logfile =~ s/\.done$//;
our $passLogFile = "$logfile.done";
our $failLogFile = "$logfile.FAIL"; # result if failure
-e $failLogFile and unlink($failLogFile);
if ( -e $passLogFile and ! $overwrite and -e $outfile )
{
	warn("Not overwriting previous results\n");
	exit;
} else
{
	unlink($passLogFile, $outfile);
}
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# COUNT INPUT
my $numInput = `grep -c '^>' $infile`;
unless ($? == 0) { die("Invalid input: $infile\n") }
chomp $numInput;
$log{NUM_INPUT} = $numInput + 0;

# CLUSTER
eval { $algorithms{$algo}->($config, $infile, $outfile, \%log, $threads); };
$@ and fail($@);

# DONE
$log{MINUTES} = int((time-$start)/60*$threads+0.5);
write_file($passLogFile, $json->pretty->encode(\%log));
exit;

sub fail
{
	my $err = shift;
	$log->{MINUTES} = int((time-$start)/60*$threads+0.5);
	$log->{ERROR} = $err;
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler { fail("INTERRUPTED: $!") }

# USEARCH CLUSTER_OTUS COMMAND WITH BUILT-IN CHIMERA DETECTION.  64bit REQUIRED.
sub usearchClusterOtus
{
	my ($config, $infile, $outfile, $log) = @_;
	my $minsize = $config->param('CLUSTERING.MIN_SIZE') or die("Min size required");
	my $minIdentity = $config->param('CLUSTERING.MIN_IDENTITY') or die("Min identity required");
	my $cmd = "$usearch -cluster_otus $infile -otus $outfile -relabel Otu -minsize $minsize -otu_radius_pct $minIdentity";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running cmd: $cmd\n$output\n");
	$log->{'NUM_CHIMERAS'} = 0;
	if ( $output =~ /\s+100.0% (\d+) OTUs, (\d+) chimeras/)
	{
		$log->{NUM_OTUS} = $1 + 0;
		$log->{NUM_CHIMERAS} = $2 + 0;
	}
	my $numOtus = `grep -c '^>' $outfile`;
	if ( $? == 0 ) { chomp $numOtus } else { $numOtus = 0 }
	$log->{NUM_OTUS} = $numOtus;
}

## VSEARCH "CLUSTER_FAST" ALGORITHM WITHOUT CHIMERA FILTERING.  MULTITHREADING SUPPORTED.
#sub vsearchCluster
#{
#	my ($config, $infile, $outfile, $log, $threads) = @_;
#	my $minSize = $config->param('VSEARCH.MIN_SIZE');
#	my $minIdentity = $config->param('VSEARCH.MIN_IDENTITY');
#	my $cmd = "vsearch --cluster_size $infile --centroids $outfile --id $minIdentity --relabel Otu --threads $threads"; # --sizeout required for uchime
#	my $output = `$cmd 2>&1`;
#	$? == 0 or die("Error running cmd: $cmd\n$output\n");
#	if ( $output =~ /Clusters: (\d+) Size min (\d+), max (\d+), avg (\d+\.\d+)/ )
#	{
#		$log->{NUM_OTUS} = $1;
#	}
#}

# VSEARCH "CLUSTER_FAST" ALGORITHM WITH "UCHIME DE NOVO" CHIMERA DETECTION AS POST-PROCESSING STEP.
sub vsearchClusterUchime
{
	my ($config, $infile, $outfile, $log, $threads) = @_;
	my $minSize = $config->param('CLUSTERING.MIN_SIZE') or die("Min size required");
	my $minIdentity = $config->param('CLUSTERING.MIN_IDENTITY') or die("Min identity required");

	# CLUSTER.  MULTITHREADING SUPPORTED.
	my ($basename) = fileparse($outfile);
	my $tmpfile = "$TMPDIR/$basename.$$.fasta";
	my $cmd = "vsearch --cluster_size $infile --centroids $tmpfile --id $minIdentity --sizeout --threads $threads";
	my $output = `$cmd 2>&1`;
	if ( $? != 0 )
	{
		-f $tmpfile and unlink($tmpfile);
		die("Error running cmd: $cmd\n$output\n");
	}
#	if ( $output =~ /Clusters: (\d+) Size min (\d+), max (\d+), avg (\d+\.\d+)/ )
#	{
#		$log->{NUM_OTUS} = $1 + 0;
#	}

	# CHIMERA FILTER.  MULTITHREADING NOT SUPPORTED.
	$cmd = "vsearch --uchime_denovo $tmpfile --nonchimeras $outfile --relabel Otu";
	$output = `$cmd 2>&1`;
	if ( $? != 0 )
	{
		-f $tmpfile and unlink($tmpfile);
		die("Error running cmd: $cmd\n$output\n");
	}
	if ( $output =~ /Found (\d+) \(\d+\.\d+%\) chimeras, (\d+) \(\d+\.\d+%\) non-chimeras,/ )
	{
		$log->{NUM_CHIMERAS} = $1 + 0;
		$log->{NUM_OTUS} = $2 + 0;
	}
	unlink($tmpfile);
}

# USEARCH UNOISE2 DENOISING ALGORITHM PRODUCES ZERO-RADIUS OTUS ("ZOTUS"). ROBERT EDGAR'S RECOMMENDED ALGORITHM.
sub usearchUnoise2
{
	my ($config, $infile, $outfile, $log) = @_;
	my $cmd = "$usearch -unoise2 $infile -fastaout $outfile";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running cmd: $cmd\n$output\n");
	$log->{NUM_CHIMERAS} = 0;
	if ($output =~ /\s+100.0% (\d+) good, (\d+) chimeras/)
	{
		#$log->{'Num OTUs'} = $1 + 0;
		$log->{NUM_CHIMERAS} = $2 + 0;
	}
	my $numOtus = `grep -c '^>' $outfile`;
	if ( $? == 0 ) { chomp $numOtus } else { $numOtus = 0 }
	$log->{NUM_OTUS} = $numOtus;
}

