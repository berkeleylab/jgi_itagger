#!/usr/bin/env perl

=pod

=head1 NAME

itaggerTaxonomy.pl

=head1 DESCRIPTION

OTU taxonomic classification via usearch utax/sintax algorithm.

=head1 OPTIONS

=over 5

=item --config C<file>

Configuration file in C<ini> format.

=item --in C<file>

OTU sequences FASTA infile.

=item --out C<file>

Taxonomic classification table outfile.

=item --log C<file>

Log outfile.  Should have C<.done> suffix, which will be replaced with C<.FAIL> upon failure.

=item --threads C<int>

Number of threads.  Optional; default=2.

=item -- C<metadata>

Optionally add metadata to logfile by providing C<key=value> pairs after a double-dash.

=back

=head1 AUTHOR

Edward Kirton (eskirton@lbl.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same license as Perl itself.

=head1 CREDITS

This pipeline wraps USEARCH; refer to http://drive5.com for documentation, license, and copyright         
information.

=cut

use warnings;
use sigtrap qw/handler signal_handler normal-signals/;
use Getopt::Long;
use Pod::Usage;
use iTagger::FastaDb;
use File::Copy;
use File::Spec qw(rel2abs);
use File::Path qw(make_path remove_tree);
use Config::Simple;
use iTagger::FastaDb;
use Env qw(TMPDIR USEARCH64 ITAGGER_CONFIG_DIR);
use File::Which;
use File::Basename;
use iTagger::Stats;
use JSON;
use File::Slurp;

our $VERSION = '3.0';
our $start = time;
our %log = ();
our $json = new JSON;
our $threads = 2;

# USEARCH
our $usearch;
if ( $USEARCH64 ) { $usearch = $USEARCH64 }
elsif ( $usearch = which('usearch64') ) { 1 }
elsif ( $usearch = which('usearch') ) { 1 }
elsif ( $usearch = which('usearch32') ) { 1 }
else { die("usearch executable not found in \$USEARCH or \$PATH\n") }
chomp $usearch;
-f $usearch or die("usearch executable not found: $usearch\n");

# OPTIONS
my $outfile;
my $configFile;
my $infile;
my $logfile;
my $overwrite;
my $help;
my $man;
GetOptions(
    'config=s' => \$configFile,
	'in=s' => \$infile,
    'out=s' => \$outfile,
	'log=s' => \$logfile,
	'threads=i' => \$threads,
	'overwrite' => \$overwrite,
    'help|?' => \$help,
    'man' => \$man,
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

# VALIDATE INPUT
$configFile or die("--config file required\n");
$logfile or die("--log file required\n");
$infile or die("--in file required\n");
$outfile or die("--out file required\n");

# LOG
$logfile =~ s/\.done$//;
our $failLogFile = "$logfile.FAIL"; # result if failure
our $passLogFile = "$logfile.done"; # result if success
-e $failLogFile and unlink($failLogFile);
if ( -e $passLogFile and ! $overwrite and -e $outfile )
{
	warn("Not overwriting previous results\n");
	#exit;
} else
{
	unlink($passLogFile, $outfile);
}
while ( my $item = shift @ARGV )
{
	my ($key,$val) = split(/=/, $item);
	$log{$key} = $val;
}

# CONFIG
my $config = new Config::Simple($configFile) or die("Invalid config file: $configFile\n");
my $algo = $config->param('TAXONOMY.METHOD');
$algo or $algo = 'UTAX';
$log{METHOD} = $algo;

# TAXONOMIC CLASSIFICATION
if ( $algo eq 'UTAX' )
{
	eval { utax($config, $infile, $outfile, \%log, $threads); };
	$@ and fail("UTAX FAILED: $@\n");
}
elsif ( $algo eq 'SINTAX' )
{
	eval { sintax($config, $infile, $outfile, \%log, $threads); };
	$@ and fail("SINTAX FAILED: $@\n");
}
else { die("Invalid classification algorithm: $algo\n") }

done();
exit;

sub done
{
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($passLogFile, $json->pretty->encode(\%log));
	exit;
}

sub fail
{
	my $err = shift;
	$log{ERROR} = $err;
	$log{MINUTES} = int((time-$start)/60*$threads+0.5);
	write_file($failLogFile, $json->pretty->encode(\%log));
	die("$err\n");
}
        
sub signal_handler
{
	fail("INTERRUPTED: $!");
	die($!);
}

sub utax
{
	my ($config, $infile, $outfile, $log, $threads) = @_;
	$infile or die("Infile required");
	my $udb = $config->param('TAXONOMY.DB');
	my $cutoff = $config->param('TAXONOMY.CUTOFF');
	$outfile or die("Outfile required");
	$threads or $threads = 1;
	$log->{METHOD} = 'UTAX';

	# RUN USEARCH
	my $tmpfile = "$outfile.tmp";
	my $cmd = "$usearch -utax $infile -db $udb -utaxout $tmpfile -strand plus -utax_cutoff $cutoff -threads $threads";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running command: $cmd\n$output\n");

	# REFORMAT OUTPUT
	my $numOtus = 0;
	my $numClassified = 0;
	my $numUnclassified = 0;
	open(my $in, '<', $tmpfile) or die($!);
	open(my $out, '>', $outfile) or die($!);
	while (<$in>)
	{
		chomp;
		++$numOtus;
		my ($id, $full, $brief, $strand) = split(/\t/);
		$brief eq '+'  and ($brief, $strand) = ($strand, $brief);
		$full =~ s/,/;/g;
		$brief =~ s/,/;/g;
		if ( $brief eq '*' )
		{
			++$numUnclassified;
			if ( $full =~ /^(\w:\w+)/ ) { $brief = $1 }
		}
		else
		{
			++$numClassified;
		}
		print $out join("\t", $id, $full, $brief, $strand), "\n";
	}
	close($in);
	close($out);
	unlink($tmpfile);
	$log->{NUM_INPUT} = $numOtus;
	$log->{NUM_CLASSIFIED} = $numClassified;
	$log->{NUM_UNCLASSIFIED} = $numUnclassified;
	$log->{PCT_CLASSIFIED} = int($numClassified/$numOtus*100+0.5);
}

sub sintax
{
	my ($config, $infile, $outfile, $log, $threads) = @_;
	$infile or die("Infile required");
	$outfile or die("Outfile required");
	my $udb = $config->param('TAXONOMY.DB');
	my $cutoff = $config->param('TAXONOMY.CUTOFF');
	$threads or $threads = 1;
	$log->{METHOD} = 'SINTAX';

	# RUN USEARCH
	my $tmpfile = "$outfile.tmp";
	my $cmd = "$usearch -sintax $infile -db $udb -tabbedout $tmpfile -strand plus -sintax_cutoff $cutoff -threads $threads";
	my $output = `$cmd 2>&1`;
	$? == 0 or die("Error running command: $cmd\n$output\n");

	# PARSE OUTPUT
	my $numOtus = 0;
	my $numClassified = 0;
	my $numUnclassified = 0;
	open(my $in, '<', $tmpfile) or die($!);
	open(my $out, '>', $outfile) or die($!);
	while (<$in>)
	{
		chomp;
		++$numOtus;
		my ($id, $full, $brief, $strand) = split(/\t/);
		$id =~ /^(Otu\d+);/ and $id = $1;
		$brief eq '+'  and ($brief, $strand) = ($strand, $brief);
		$full =~ s/,/;/g;
		$brief =~ s/,/;/g;
		if ( $brief eq '*' )
		{
			++$numUnclassified;
			if ( $full =~ /^(\w:\w+)/ ) { $brief = $1 }
		}
		else
		{
			++$numClassified;
		}
		print $out join("\t", $id, $full, $brief, $strand), "\n";
	}
	close($in);
	close($out);
	unlink($tmpfile);
	$log->{NUM_INPUT} = $numOtus;
	$log->{NUM_CLASSIFIED} = $numClassified;
	$log->{NUM_UNCLASSIFIED} = $numUnclassified;
	$log->{PCT_CLASSIFIED} = int($numClassified/$numOtus*100+0.5);
}

__END__
