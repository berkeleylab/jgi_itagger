=pod

=head1 NAME

Fasta - Simple object for Fasta sequence

=head1 SYNOPSIS

    my $seq=new Fasta( $hdr, $seq );
    $seq->qc;
    print $seq->output;

=head1 DESCRIPTION

Object for a single read sequence, with methods for basic manipulation and quality control.

=head1 METHODS

=over 5

=cut

package iTagger::FastaDb::Fasta;

use warnings;
use strict;
use Carp;
use constant {
    CHARACTERS_PER_LINE => 80, # for formatting Fasta output
    CLOSE_ENOUGH_TO_END => 6,   # hits of adapters this close to end will result in trimming to be done to end
};

our $VERSION = 1.0;

=item new $hdr $seq

Initialize new sequence object.  Note: we are using /^>(\S+)/ as the ID, not /^>(\w+)/ as some folks do.

=cut

sub new
{
    my ($class,$hdr,$seq)=@_;
    confess("Incomplete Fasta record: hdr=$hdr, seq=$seq\n") unless $hdr and $seq;
	chomp $seq;
	$hdr = '>' . $hdr unless $hdr =~ /^>/;
	$hdr =~ /^>(\S+)(.*)$/ or confess("Invalid FASTA header: $hdr\n");
    my $this=
	{
		id => $1,
		info => $2,
        seq => $seq
    };
    bless $this,$class;
    return $this;
}

=item header <$idOnly>

Returns the object's header line.  If $idOnly is true then will output only the ID, not the complete header.

=cut

sub header
{
    my ($this, $idOnly)=@_;
    if ( $idOnly or ! $this->{info} ) { return '>'.$this->{id}."\n" }
	else { return '>'.$this->{id}.' '.$this->{info}."\n" }
}

=item id

Returns the object's ID, which is the sequence's unique identifier without comments which may be present in the header.  It may be changed by passing the new ID as the optional argument.

=cut

sub id
{
	my ($this, $id) = @_;
	if ( defined($id) and length($id) ) { return $this->{id}=$id }
	else { return $this->{id} }
}

=item info

Returns the extra text that may be included in a FASTA header.  May be changed by providing new string.

=cut

sub info
{
	my ($this, $info) = @_;
	$info or return $this->{info};
	return $this->{info}=$info;
}

=item seq ($new_seq)

Returns the read's complete sequence, without newlines.  Optional argument changes it.

=cut

sub seq
{
    my ($this, $seq)=@_;
	$seq or return $this->{seq};
	chomp $seq;
	$seq =~ s/\n//g;
	return $this->{seq}=$seq;
}

=item revcomp

Reverse-complements a sequence.

=cut

sub revcomp
{
    my $this=shift;
    return unless $this->{seq};
    $this->{seq} =~ tr/ATCGatcg/TAGCtagc/;
    my @seq=reverse split(//, $this->{seq});
    $this->{seq}=join('', @seq);
}

=item output

Returns a multiline string of the sequence in Fasta format.  Returns no output if sequence is empty.  Optional idOnly flag will result in header containing only the ID, not any additional text that was originally present.

=cut

sub output
{
    my ($this, $idOnly) = @_;
    return '' unless $this->{seq}; # will be empty if failed QC
    return $this->header($idOnly) . _format($this->{seq});
}

# PRIVATE METHOD TO ADD NEWLINE CHARACTERS 
sub _format
{
    my $old=shift;
    return '' unless $old;
    return $old unless CHARACTERS_PER_LINE;
    my $new='';
    while (length($old)> CHARACTERS_PER_LINE) {
        $new .= substr($old,0, CHARACTERS_PER_LINE)."\n";
        $old = substr($old, CHARACTERS_PER_LINE);
    }
    $old and $new .= $old."\n";
    return $new;
}

=item len

Returns length of the seq

=cut

sub len
{
    my $this = shift;
    return length($this->{seq});
}

=item qc $winsize $meanq $minlen $maxn

To perform minimum length filtering, and filtering reads with too many Ns.

=cut

sub qc
{
    my ($this, $minlen, $maxn)=@_;
    $this->trim_terminal_Ns;
    $this->length_filter($minlen);
    $this->N_filter($maxn);
}

=item trim_terminal_Ns

Discard uninformative Ns from the ends of the sequence.

=cut

sub trim_terminal_Ns
{
    my $this=shift;
	my $seq = $this->{seq};
	$seq =~ s/^N+//;
	$seq =~ s/N+$//;
	$this->{seq} = $seq;
}

=item length_filter

If the sequence is shorter than the minimum length, the sequence string is emptied so they will not be
returned by the output method.  Returns true if sequence was filtered.

=cut

sub length_filter
{
    my ($this, $minlen)=@_;
	!defined($minlen) and return 0;
    if ( $this->len < $minlen)
	{ 
        $this->{seq}='';
        return 1;
    }
	else
	{
        return 0;
    }
}

=item N_filter

If the sequence contains more than the allowed number of Ns, the sequence string is emptied.
Returns true if sequence was filtered.

=cut

sub N_filter
{
    my ($this,$maxn)=@_;
    return 1 unless defined($maxn);
    # COUNT NUMBER OF 'N' BASES IN TRIMMED SEQ
    my $seq=$this->{seq};
    my $n= $seq=~ s/N//gi;
    if ($n <= $maxn)
	{
        return 0;
    }
	else
	{
        # FAIL
        $this->{seq}='';
        return 1;
    }
}

=item low_complexity_filter $pct_len

If the sequence is >= $pct_len mono- or di-nucleotide repeats, clears the sequence string and returns true.

=cut

sub low_complexity_filter
{
    my ($this, $pct_len)=@_;
    my $len = $this->len;
    foreach my $n (qw/A T C G/)
	{
		my $seq = $this->{seq};    
        my $n = $seq =~ s/$n//g;
        if ($n >= $pct_len/100*$len)
		{
			$this->{seq}='';
			return 1;
        }
    }
    foreach my $nn (qw/AT AC AG TA TC TG CA CT CG GA GT GC/)
	{
		my $seq = $this->{seq};    
        my $n = $seq =~ s/$nn//g;
        if ($n >= $pct_len/200*$len)
		{
			$this->{seq}='';
			return 1;
        }
    }
    return 0;
}

=item subseq

Get substring of sequence.

=cut

sub subseq
{
	my ($this, $offset, $len) = @_;
	$offset or $offset = 0;
	$this->{seq} or return 0;
	if ( !defined($len) )
	{
		if ( $offset > $this->len )
		{
			warn("Unable to get substr($offset) of id=".$this->id.' (length=)'.$this->len.")\n");
			$this->del('substr_err');
			return 0;
		} else
		{
			$this->{seq} = substr($this->seq, $offset);
			return 1;
		}
	} elsif ( $this->len >= ( $offset + $len ))
	{
		$this->{seq} = substr($this->seq, $offset, $len);
		return 1;
	} elsif ( $offset < $this->len )
	{
		warn("Unable to get substr($offset, $len) or subseq(${offset}..".($offset + $len - 1) .") of id=".$this->id.' (length='.$this->len.")\n");	
		$this->{seq} = substr($this->seq, $offset);
		$this->del('subtr_err');
		return 0;
	} else
	{
		warn("Unable to get substr($offset, $len) or subseq(${offset}..".($offset + $len - 1) .") of id=".$this->id.' (length='.$this->len.")\n");	
		$this->del('subtr_err');
		return 0;
	}
}

=back

=head1 COPYRIGHT

Copyright (c) 2010 U.S. Department of Energy Joint Genome Institute

All right reserved. This program is free software; you can redistribute it
and/or modify it under the same terms as Perl itself.

=head1 AUTHOR

Edward Kirton <ESKirton@LBL.gov>

=cut

1;
__END__
