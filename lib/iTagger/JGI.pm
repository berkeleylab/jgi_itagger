=pod

=head1 NAME

iTagger::JGI

=head1 DESCRIPTION

For JGI internal use only; queries JGI JAMO database.

=head1 FUNCTIONS

=over 5

=cut

package iTagger::JGI;

use strict;
use File::Which;
use JSON;
#use Slurp;

our $jamo = which('jamo');
if ( $jamo )
{
	chomp $jamo;
} else
{
	die("JAMO not found (for JGI internal use only; did you \"module load jamo\" first?)\n");
}

=item project

Query database given JGI Project ID.

=cut

sub project
{
	my $id = shift;

	my %sampleNames; # name => id;
	my $output = `$jamo report select metadata.sow_segment.sample_id, metadata.sow_segment.sample_name, file_path, file_name where metadata.sequencing_project_id = $id and metadata.fastq_type = sdm_normal as json`;
	$? == 0 or die("Error querying JAMO\n");
	chomp $output;
	my $ar  = decode_json $output;
	my @result;
	my $notFound = 0;
	foreach my $hr (@$ar)
	{
		exists ($hr->{'metadata.sow_segment.sample_id'}) or die("Invalid JSON file\n");
		exists ($hr->{'metadata.sow_segment.sample_name'}) or die("Invalid JSON file\n");
		exists ($hr->{'file_path'}) or die("Invalid JSON file\n");
		exists ($hr->{'file_name'}) or die("Invalid JSON file\n");
		my $sampleId = $hr->{'metadata.sow_segment.sample_id'};
		my $sampleName = $hr->{'metadata.sow_segment.sample_name'};
		my $path = $hr->{'file_path'};
		my $file = $hr->{'file_name'};
		$sampleName or die("Sample name not defined for $sampleId\n");
		my $sampleName0 = $sampleName;
		$sampleName =~ s/\W//g;
		if ( $sampleName ne $sampleName0 )
		{
			warn("Sample $sampleName0 renamed to $sampleName (invalid chars)\n");
			$sampleName0 = $sampleName;
		}
		if ( $sampleName !~ /^[a-z]/i )
		{
			$sampleName = "SAMPLE_$sampleName";
			warn("Sample $sampleName0 renamed to $sampleName (for QIIME)\n");
		}
		if ( exists($sampleNames{$sampleName}) )
		{
			if ( $sampleNames{$sampleName} != $sampleId )
			{
				die("Sample names not unique for $sampleId and ".$sampleNames{$sampleName}.": $sampleName\n");
			}
		} else
		{
			$sampleNames{$sampleName} = $sampleId;
		}
		unless ( -f "$path/$file" )
		{
			my $output = `$jamo fetch filename $file`;
			warn($output);
			++$notFound;
		}
		push @result, [ $sampleName, "$path/$file" ];
	}
	if ( $notFound )
	{
		die("$notFound fastq files were not found\n");
	}
	return \@result;
}

=item validatePrimerSet

Confirm all input files have the same primer set, which is returned.

=cut

sub validatePrimerSet
{
	my $inputAR = shift;
	my $primerSet = undef;
	my ($name, $path, $basename, $folder, $suffix, $filename, $path1, $status, $id, $primerSet1);
	foreach my $row (@$inputAR)
	{
		($name, $path) = @$row;
		($basename, $folder, $suffix) = fileparse($path, qr/\.[^.]*/);
		$filename = "$basename$suffix";
		($path1, $status, $id) = info($filename);
		$primerSet1 = itagPrimerSet($id);
		defined($primerSet1) or die("itag_primer_set not defined for $name\n");
		defined($primerSet) or $primerSet = $primerSet1;
		$primerSet eq $primerSet1 or die("itag_primer_set not all the same!\n");
	}
	return $primerSet;
}

=item info

Return complete path, status, and ID.

=cut

sub info
{
	my $filename = shift;
	$filename or die("Filename required\n");
	my $output=`$jamo info filename $filename`;
	chomp $output;
	my @rec = split(/\s+/, $output); # filename, completePath, status, ID
	return @rec[1..3];
}

=item itagPrimerSet

Return primer string given an ID.

=cut

sub itagPrimerSet
{
	my $id = shift;
	$id or die("ID required\n");
	my $output=`$jamo show id | grep itag_primer_set`;
	return ( $output and $output =~ /itag_primer_set: (\S+)/ ) ? $1 : undef;
}

=item inputJson

Input JSON sample file.

=cut

sub inputJson
{
	my $file = shift;
	#my $txt = slurp($file);
	my $txt = '';
	open(my $in, '<', $file) or die($!);
	while (<$in>) { $txt .= $_ }
	close($in);

	my $rec = decode_json($txt);	
	my @result;
	foreach my $hr ( @$rec )
	{
		exists($hr->{'metadata.sample_name'}) or die("Missing metadata.sample_name\n");
		exists($hr->{'metadata.seq_unit_name'}) or die("Missing metadata.seq_unit_name\n");
		exists($hr->{'file_path'}) or die("Missing file_path\n");
		exists($hr->{'file_name'}) or die("Missing file_name\n");
		push @result,
		[
			$hr->{'metadata.sample_name'},
			$hr->{'metadata.seq_unit_name'},
			$hr->{'file_path'}.'/'.$hr->{'file_name'}
		];
	}
	return \@result;
}

1;

=back

=head1 AUTHOR/SUPPORT

Edward Kirton (ESKirton@LBL.gov)

=cut
