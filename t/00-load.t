#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

plan tests => 3;

BEGIN {
    use_ok( 'iTagger::FastaDb' ) || print "Bail out!\n";
    use_ok( 'iTagger::FastqDb' ) || print "Bail out!\n";
    use_ok( 'iTagger::Stats' ) || print "Bail out!\n";
}

diag( "Testing iTagger::FastaDb $iTagger::FastaDb::VERSION, Perl $], $^X" );
diag( "Testing iTagger::FastqDb $iTagger::FastqDb::VERSION, Perl $], $^X" );
diag( "Testing iTagger::Stats $iTagger::Stats::VERSION, Perl $], $^X" );
